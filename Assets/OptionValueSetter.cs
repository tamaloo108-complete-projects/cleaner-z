﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class OptionValueSetter : MonoBehaviour
{
    public TMP_Dropdown _QualityDropdown;
    public Toggle _Bloom;
    public Toggle _PostProcessing;
    public Slider _MasterVol;
    public Slider _SFXVol;
    public Slider _BGMVol;
    public Button ApplyButton;
    public Button CloseButton;
}
