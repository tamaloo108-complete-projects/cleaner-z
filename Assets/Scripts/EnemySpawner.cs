﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System;

//Particle List:
// 0 bottom light indicator
// 1 damage canvas enemy
// 2 damage canvas tank
// 3 Atk up
// 4 Spd up
// 5 Pow up
// 6 Range up
// 7 heal
// 8 burst 1
// 9 burst 2

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] SpawnerData MySpawnData;
    [SerializeField] Collider SpawnArea;
    [SerializeField] float MinRandomizer = -100f;
    [SerializeField] float MaxRandomizer = 100f;
    [SerializeField] float SpawnTimeRandomizer = 3f;
    float TimeBetweenWave = 3f;
    int EnemyPerWave = 20;

    [SerializeField] int MaxEnemyOnScreen = 8;

    int curEnemyIndex = 0;
    int WaveCount = 0;
    int curEnemyCount = 0;
    int curBossWaveCount = 0;
    int curEnemyWaveCount = 0;
    int _onWave = -1;
    int curBossOnScreen = -1;
    int _BossTemp = -1;
    int curBossIndex = 0;
    bool SpawnBoss = false;
    bool BossSpawned = false;
    Vector3 CurrentSpawnPosition = Vector3.zero;

    [SerializeField] private List<AssetReference> _MobReferences = new List<AssetReference>();
    [SerializeField] private List<AssetReference> _ParticleReferences = new List<AssetReference>();

    private readonly Dictionary<AssetReference, List<GameObject>> _SpawnedMobs = new Dictionary<AssetReference, List<GameObject>>();
    private readonly Dictionary<AssetReference, List<GameObject>> _SpawnedParticles = new Dictionary<AssetReference, List<GameObject>>();
    private readonly Dictionary<AssetReference, Queue<Vector3>> _queueSpawnReq = new Dictionary<AssetReference, Queue<Vector3>>();
    private readonly Dictionary<AssetReference, Queue<Transform>> _queueSpawnParticleReq = new Dictionary<AssetReference, Queue<Transform>>();
    private readonly Dictionary<AssetReference, Queue<Vector3>> _queueSpawnParticleReqVec = new Dictionary<AssetReference, Queue<Vector3>>();
    private readonly Dictionary<AssetReference, AsyncOperationHandle<GameObject>> _asyncOperationHandle = new Dictionary<AssetReference, AsyncOperationHandle<GameObject>>();

    public int _WavesMobs { get; private set; }

    Coroutine EnemySpawnerCoroutine;
    Coroutine BossCounterTimerCoroutine;
    // Start is called before the first frame update
    private void OnEnable()
    {
        CountTotalMobs();
    }

    void Start()
    {
        WaveCount = MySpawnData.Wave.Count;


        if (EnemySpawnerCoroutine == null)
            EnemySpawnerCoroutine = StartCoroutine(EnemyOnSpawn());


        MixedHandler.Instance.SetEnemySpawner(this);

    }


    private void CountTotalMobs()
    {
        _WavesMobs = -1;
        foreach (var z in MySpawnData.Wave)
        {
            for (int i = 0; i < z._MobWaveCount; i++)
            {
                _WavesMobs++;
            }

            for (int i = 0; i < z.MaxBossOnScreen; i++)
            {
                _WavesMobs++;
            }
        }
    }

    public void SpawnParticle(int index, Vector3 offset)
    {

        if (index < 0 || index > _ParticleReferences.Count)
            return;

        AssetReference _assetreference = _ParticleReferences[index];

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            return;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
            {
                SpawnParticlesFromLoaderReference(_assetreference, offset);
            }
            else
                EnqueueSpawnParticlesForAfterInitialization(_assetreference, offset);
            return;
        }

        LoadAndSpawnParticle(_assetreference, offset);
    }


    public void SpawnParticle(int index, Transform parent)
    {

        if (index < 0 || index > _ParticleReferences.Count)
            return;

        AssetReference _assetreference = _ParticleReferences[index];

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            return;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
            {
                if (index == 1)
                    SpawnParticlesFromLoaderReference(_assetreference, parent, MixedHandler.Instance.curDmgText);
                else if (index == 2)
                    SpawnParticlesFromLoaderReference(_assetreference, parent, MixedHandler.Instance.curDmgTankTaken);
                else
                    SpawnParticlesFromLoaderReference(_assetreference, parent);
            }
            else
                EnqueueSpawnParticlesForAfterInitialization(_assetreference, parent);
            return;
        }

        LoadAndSpawnParticle(_assetreference, parent);
    }

    public void SpawnParticle(PowerUpTypes index, Transform parent)
    {
        AssetReference _assetreference = GetAssetReference(index);

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            return;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
            {
                SpawnParticlesFromLoaderReference(_assetreference, parent);
            }
            else
                EnqueueSpawnParticlesForAfterInitialization(_assetreference, parent);
            return;
        }

        LoadAndSpawnParticle(_assetreference, parent);
    }


    private AssetReference GetAssetReference(PowerUpTypes types)
    {
        switch (types)
        {
            case PowerUpTypes.AttackUp:
                return _ParticleReferences[3];

            case PowerUpTypes.SpeedUp:
                return _ParticleReferences[4];

            case PowerUpTypes.PowerUp:
                return _ParticleReferences[5];

            case PowerUpTypes.RangeUp:
                return _ParticleReferences[6];

            default:
                return _ParticleReferences[7];
        }
    }

    private IEnumerator EnemyOnSpawn()
    {
        while (true)
        {
            yield return new WaitUntil(() => MixedHandler.Instance.DoSpawnEnemy);
            yield return new WaitUntil(() => !MixedHandler.Instance.AllFreeze);
            yield return new WaitUntil(() => !MixedHandler.Instance.GameOnPause);

            if (WaveCount > 0)
            {
                if (MySpawnData.Wave[_onWave < 0 ? 0 : _onWave]._BossBattle && !SpawnBoss)
                {
                    if (BossCounterTimerCoroutine == null)
                    {
                        BossCounterTimerCoroutine = StartCoroutine(BossTimerSpawner());
                    }
                }

                if (curEnemyWaveCount == 0)
                {

                    if (WaveCount == 0) break;

                    if (curEnemyCount == 0)
                    {
                        //Next Wave
                        WaveCount -= 1;
                        _onWave = (int)Mathf.MoveTowards(_onWave, MySpawnData.Wave.Count - 1, 1);
                        EnemyPerWave = MySpawnData.Wave[_onWave]._MobWaveCount;
                        curEnemyWaveCount = EnemyPerWave;
                        curBossWaveCount = MySpawnData.Wave[_onWave < 0 ? 0 : _onWave].MaxBossOnScreen;
                        curBossOnScreen = MySpawnData.Wave[_onWave]._BossBattle ? 0 : 0;
                        //SpawnBoss = false;
                        BossSpawned = false;
                        _BossTemp = -1;
                        //curBossIndex = 0;
                        //curBossOnScreen = 0;

                    }


                    //yield return RandomizerTime(TimeBetweenWave);
                }

                //if (SpawnBoss && curBossWaveCount > 0 && curBossOnScreen < MySpawnData.Wave[_onWave].MaxBossOnScreen)
                //{
                //    yield return BossSpawn();
                //    RandomizeSpawnPosition();
                //    yield return new WaitUntil(() => !MixedHandler.Instance.BossEntrance);
                //    yield return RandomizerTime(SpawnTimeRandomizer);
                //    yield return Spawn(curEnemyIndex);
                //    curBossWaveCount -= 1;
                //}

                //yield return new WaitUntil(() => !SpawnBoss);

                if (!SpawnBoss && curEnemyCount < MaxEnemyOnScreen && (curEnemyWaveCount) > 0)
                {
                    RandomizeSpawn();
                    RandomizeSpawnPosition();
                    yield return new WaitUntil(() => !MixedHandler.Instance.BossEntrance);
                    yield return RandomizerTime(SpawnTimeRandomizer);
                    yield return Spawn(curEnemyIndex);
                    curEnemyWaveCount -= 1;
                }
            }
            else if (WaveCount <= 0)
            {
                if (curEnemyCount == 0)
                {
                    MixedHandler.Instance.WeWon = true;
                    MixedHandler.Instance.GameIsOver = true;
                }
            }
        }

        if (WaveCount <= 0)
        {
            if (curEnemyCount == 0)
            {
                MixedHandler.Instance.WeWon = true;
                MixedHandler.Instance.GameIsOver = true;
            }
        }
    }

    private IEnumerator BossTimerSpawner()
    {
        Debug.Log("start waiting. ");
        if (curBossOnScreen >= MySpawnData.Wave[_onWave].MaxBossOnScreen && curBossWaveCount <= 0)
        {
            BossCounterTimerCoroutine = null;
            yield break;
        }

        var k = MySpawnData.Wave[_onWave].BossTimer;
        while (k > 0)
        {
            yield return new WaitUntil(() => MixedHandler.Instance.BossEntrance == false);
            yield return new WaitForSeconds(.1f);
            k -= .1f;

            if (k <= 0)
            {
                if (!SpawnBoss && curBossOnScreen < MySpawnData.Wave[_onWave].MaxBossOnScreen && curBossWaveCount > 0)
                {
                    SpawnBoss = true;
                    curBossWaveCount -= 1;
                    RandomizeSpawnPosition();
                    yield return BossSpawn();

                    yield return new WaitUntil(() => !MixedHandler.Instance.BossEntrance);
                    yield return RandomizerTime(SpawnTimeRandomizer);
                    yield return Spawn(curEnemyIndex);

                    BossCounterTimerCoroutine = null;
                    yield break;
                }
            }
        }

        //SpawnBoss = true;
        //if (SpawnBoss && curBossOnScreen < MySpawnData.Wave[_onWave].MaxBossOnScreen && curBossWaveCount > 0)
        //{
        //    RandomizeSpawnPosition();
        //    yield return BossSpawn();

        //    yield return new WaitUntil(() => !MixedHandler.Instance.BossEntrance);
        //    yield return RandomizerTime(SpawnTimeRandomizer);
        //    yield return Spawn(curEnemyIndex);
        //    curBossWaveCount -= 1;

        //}
        BossCounterTimerCoroutine = null;
        Debug.Log("spawn ");
        yield break;
    }

    private IEnumerator RandomizerTime(float t)
    {
        var v = UnityEngine.Random.Range(0f, t);
        //   while (v > 0)
        //  {
        yield return new WaitForSeconds(v);
        // v -= 0.1f;
        //  }
        yield break;
    }

    IEnumerator BossSpawn()
    {
        if (MySpawnData.Wave[_onWave]._BossIndex.Count != 1)
        {
            _BossTemp = (int)Mathf.MoveTowards(_BossTemp, MySpawnData.Wave[_onWave]._BossIndex.Count, 1);
            curBossIndex = MySpawnData.Wave[_onWave]._BossIndex[_BossTemp];
        }
        else
        {
            curBossIndex = MySpawnData.Wave[_onWave]._BossIndex[0];
        }
        yield return new WaitForEndOfFrame();
        curEnemyIndex = curBossIndex;
        //

    }

    void RandomizeSpawn()
    {
        curEnemyIndex = UnityEngine.Random.Range(0, MySpawnData.Wave[_onWave]._MobIndex.Count);
    }

    void RandomizeSpawnPosition()
    {
        var _temp = new Vector3(UnityEngine.Random.Range(MinRandomizer, MaxRandomizer), 1f, UnityEngine.Random.Range(MinRandomizer, MaxRandomizer));
        CurrentSpawnPosition = Physics.ClosestPoint(_temp, SpawnArea, SpawnArea.transform.position, SpawnArea.transform.rotation);
        CurrentSpawnPosition = new Vector3(CurrentSpawnPosition.x, 1.3f, CurrentSpawnPosition.z);
    }


    IEnumerator Spawn(int index)
    {
        if (index < 0 || index > _MobReferences.Count)
            yield break;

        AssetReference _assetreference = _MobReferences[index];

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            yield break;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
                SpawnFromLoaderReference(_assetreference, CurrentSpawnPosition);
            else
                EnqueueSpawnForAfterInitialization(_assetreference, CurrentSpawnPosition);
            yield break;
        }

        LoadAndSpawn(_assetreference, CurrentSpawnPosition);

        yield break;
    }

    private void LoadAndSpawn(AssetReference assetreference, Vector3 currentSpawnPosition)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnFromLoaderReference(assetreference, currentSpawnPosition);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnFromLoaderReference(assetreference, currentSpawnPosition);
                }
            }
        };
    }

    private void EnqueueSpawnForAfterInitialization(AssetReference assetreference, Vector3 currentSpawnPosition)
    {
        if (_queueSpawnReq.ContainsKey(assetreference) == false)
        {
            _queueSpawnReq[assetreference] = new Queue<Vector3>();
        }

        _queueSpawnReq[assetreference].Enqueue(currentSpawnPosition);
    }

    private void SpawnFromLoaderReference(AssetReference assetreference, Vector3 currentSpawnPosition)
    {
        assetreference.InstantiateAsync(currentSpawnPosition, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_SpawnedMobs.ContainsKey(assetreference) == false)
            {
                _SpawnedMobs[assetreference] = new List<GameObject>();
            }

            _SpawnedMobs[assetreference].Add(asyncOperationHandle.Result);
            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();

            SpawnParticle(0, asyncOperationHandle.Result.transform);
            //if (!SpawnBoss)
            curEnemyCount += 1;
            notify.Destroyed += Notify_Destroyed;
            notify.AssetReference = assetreference;

            if (SpawnBoss)
            {
                curBossOnScreen = (int)Mathf.MoveTowards(curBossOnScreen, MySpawnData.Wave[_onWave].MaxBossOnScreen, 1);
                var g = asyncOperationHandle.Result.GetComponent<CharacterHandler>();
                MixedHandler.Instance.Boss.Add(g);
                MixedHandler.Instance.BossEntrance = true;
                SpawnBoss = false;

                ///BossSpawned = false;
            }
        };
    }

    private Vector3 GameEventObserver_OnGetSpawnPosition()
    {
        return CurrentSpawnPosition;
    }

    private void LoadAndSpawnParticle(AssetReference assetreference, Transform parent)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnParticlesFromLoaderReference(assetreference, parent);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnParticlesFromLoaderReference(assetreference, parent);
                }
            }
        };
    }

    private void LoadAndSpawnParticle(AssetReference assetreference, Vector3 parent)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnParticlesFromLoaderReference(assetreference, parent);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnParticlesFromLoaderReference(assetreference, parent);
                }
            }
        };
    }

    private void EnqueueSpawnParticlesForAfterInitialization(AssetReference assetreference, Vector3 parent)
    {
        if (_queueSpawnParticleReq.ContainsKey(assetreference) == false)
        {
            _queueSpawnParticleReqVec[assetreference] = new Queue<Vector3>();
        }

        _queueSpawnParticleReqVec[assetreference].Enqueue(parent);
    }

    private void EnqueueSpawnParticlesForAfterInitialization(AssetReference assetreference, Transform parent)
    {
        if (_queueSpawnParticleReq.ContainsKey(assetreference) == false)
        {
            _queueSpawnParticleReq[assetreference] = new Queue<Transform>();
        }

        _queueSpawnParticleReq[assetreference].Enqueue(parent);
    }


    private void SpawnParticlesFromLoaderReference(AssetReference assetreference, Transform ObjectPosition)
    {
        assetreference.InstantiateAsync(ObjectPosition, false).Completed += (asyncOperationHandle) =>
         {
             if (_SpawnedParticles.ContainsKey(assetreference) == false)
             {
                 _SpawnedParticles[assetreference] = new List<GameObject>();
             }

             _SpawnedParticles[assetreference].Add(asyncOperationHandle.Result);

             var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
             var obj = asyncOperationHandle.Result.transform;

             notify.Destroyed += Notify_SpawnDestroyed;
             notify.AssetReference = assetreference;
         };
    }

    private void SpawnParticlesFromLoaderReference(AssetReference assetreference, Vector3 ObjectPosition)
    {
        assetreference.InstantiateAsync(ObjectPosition, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_SpawnedParticles.ContainsKey(assetreference) == false)
            {
                _SpawnedParticles[assetreference] = new List<GameObject>();
            }

            _SpawnedParticles[assetreference].Add(asyncOperationHandle.Result);

            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            var obj = asyncOperationHandle.Result.transform;

            Destroy(asyncOperationHandle.Result, 2f);

            notify.Destroyed += Notify_SpawnDestroyed;
            notify.AssetReference = assetreference;
        };
    }

    private void SpawnParticlesFromLoaderReference(AssetReference assetreference, Transform ObjectPosition, string name)
    {
        assetreference.InstantiateAsync(ObjectPosition, false).Completed += (asyncOperationHandle) =>
        {
            if (_SpawnedParticles.ContainsKey(assetreference) == false)
            {
                _SpawnedParticles[assetreference] = new List<GameObject>();
            }

            var k = asyncOperationHandle.Result.GetComponent<ChangeDamageText>();
            k.SetPosition(ObjectPosition.position);
            k.SetTextDMG(name);

            if (k == null) //settting particle end, if is not text, since text has built-in automatic destroy function.
                Destroy(asyncOperationHandle.Result, 2f);

            _SpawnedParticles[assetreference].Add(asyncOperationHandle.Result);

            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            var obj = asyncOperationHandle.Result.transform;

            notify.Destroyed += Notify_SpawnDestroyed;
            notify.AssetReference = assetreference;
        };
    }

    private void Notify_Destroyed(AssetReference assetReference, NotifyOnDestroy obj)
    {
        if (GameEventObserver.SpawnPreferences == PowerUpSpawnPreferences.AfterKill)
        {
            //notify there is kill.
            GameEventObserver.DoSpawnPowerUp = true;
            GameEventObserver._GetPosition = obj.transform.position;
        }

        Addressables.ReleaseInstance(obj.gameObject);

        _SpawnedMobs[assetReference].Remove(obj.gameObject);
        if (_SpawnedMobs[assetReference].Count == 0)
        {
            if (_asyncOperationHandle[assetReference].IsValid())
            {
                Addressables.Release(_asyncOperationHandle[assetReference]);
            }

            _asyncOperationHandle.Remove(assetReference);
        }

        curEnemyCount -= 1;
        _WavesMobs -= 1;
    }

    private void Notify_SpawnDestroyed(AssetReference assetReference, NotifyOnDestroy obj)
    {
        Addressables.ReleaseInstance(obj.gameObject);

        _SpawnedParticles[assetReference].Remove(obj.gameObject);
        if (_SpawnedParticles[assetReference].Count == 0)
        {
            if (_asyncOperationHandle[assetReference].IsValid())
            {
                Addressables.Release(_asyncOperationHandle[assetReference]);
            }

            _asyncOperationHandle.Remove(assetReference);
        }
    }
}
