﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iNPCBillboardCanvas
{
    void BillboardCanvas(Transform target);
}

public class NpcCanvasBillboard : MonoBehaviour
{
    [SerializeField] Camera ReferenceCamera;
    public enum Axis { up, down, left, right, forward, back }
    public bool ReverseFace = false;
    public Axis axis = Axis.up;
    Coroutine FindCorrectCam;

    public Vector3 GetAxis(Axis refAxis)
    {
        switch (refAxis)
        {
            case Axis.up:
                return Vector3.up;

            case Axis.down:
                return Vector3.down;

            case Axis.left:
                return Vector3.left;

            case Axis.right:
                return Vector3.right;

            case Axis.back:
                return Vector3.back;
        }

        return Vector3.up;
    }

    private void Start()
    {
        if (!ReferenceCamera)
        {
            if (FindCorrectCam == null)
                FindCorrectCam = StartCoroutine(DoFindCorrectCam());
        }
    }

    private IEnumerator DoFindCorrectCam()
    {
        while (true)
        {
            if (ReferenceCamera != null)
            {
                FindCorrectCam = null;
                yield break;
            }
            yield return new WaitForEndOfFrame();
            ReferenceCamera = Camera.main;
        }
    }

    private void Update()
    {
        BillboardCanvas(transform);
    }

    void BillboardCanvas(Transform target)
    {
        if (!ReferenceCamera) return;
        Vector3 targetPos = target.position + ReferenceCamera.transform.rotation * (ReverseFace ? Vector3.forward : Vector3.back);
        Vector3 targetOrientation = ReferenceCamera.transform.rotation * GetAxis(axis);
        target.LookAt(targetPos, targetOrientation);
    }

}

