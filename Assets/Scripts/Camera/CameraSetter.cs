﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityStandardAssets.CrossPlatformInput;

public class CameraSetter : MonoBehaviour
{
    [SerializeField] CinemachineTargetGroup TargetGroup;
    [SerializeField] float PlayerTargetGroupWeight = 1f;
    [SerializeField] float PlayerTargetGroupRadius = 0.3f;
    [SerializeField] float BossEntranceTime = 1.5f;
    float curBossEntranceTime;

    [Header("Cinemachine Camera Settings")]
    [SerializeField] CinemachineVirtualCamera StartCamera;
    [SerializeField] CinemachineVirtualCamera CinematicCamera;
    [SerializeField] CinemachineVirtualCamera PauseCamera;
    [SerializeField] Animator CameraAnimator;

    [Header("HUD Layout Settings")]
    [SerializeField] GameObject ControllerHUD;
    [SerializeField] GameObject GeneralHUD;
    [SerializeField] GameObject MainMenuHUD;
    [SerializeField] Animator BossEntranceSpecialAnimator;

    [SerializeField] List<CharacterHandler> Players;
    Transform CurSelectedPlayer;
    int CurIndex = 0;

    bool onEntrance = false;
    CharacterHandlerProcessor chap;

    private void OnEnable()
    {
        CurSelectedPlayer = Players[CurIndex].transform;
    }

    void Start()
    {
        //FindPlayers();
        curBossEntranceTime = BossEntranceTime;
        chap = new CharacterHandlerProcessor();
    }

    public CharacterHandler GetCurrentPlayer()
    {
        return CurSelectedPlayer.GetComponent<CharacterHandler>();
    }

    void Update()
    {
        DetectSwipe();
        DetectStart();
        DetectBoss();
        DetectWin();
    }

    private void DetectWin()
    {
        if (MixedHandler.Instance.WeWon || MixedHandler.Instance.GameIsOver)
        {
            CameraAnimator.SetBool("Start", false);
        }
    }

    private void DetectBoss()
    {
        if (MixedHandler.Instance.Boss.Count <= 0)
        {
          //  BossEntranceSpecialAnimator.SetBool("Active", false);
        }

        if (onEntrance)
        {
            if (curBossEntranceTime <= 0)
            {
                onEntrance = false;
                MixedHandler.Instance.BossEntrance = false;
                CameraAnimator.SetBool("Cinematic", false);
                curBossEntranceTime = BossEntranceTime;
            }
            curBossEntranceTime -= Time.deltaTime;
        }

        if (MixedHandler.Instance.BossEntrance && !onEntrance)
        {
            onEntrance = true;
            SetCinematicCamTarget(MixedHandler.Instance.Boss[MixedHandler.Instance.Boss.Count - 1].transform);
            CameraAnimator.SetBool("Cinematic", onEntrance);
           // BossEntranceSpecialAnimator.SetBool("Active", true);
        }
    }

    private void DetectStart()
    {
        if (OnClickStart())
        {
            GameStart(true);
        }
    }

    private void GameStart(bool v)
    {
        DisablePlayer();
        MainMenuHUD.SetActive(false);
        ControllerHUD.SetActive(true);
        GeneralHUD.SetActive(true);
        CameraAnimator.SetBool("Start", v);
        AddToTargetGroup();

        var t = CurSelectedPlayer.GetComponent<CharacterHandler>();
        chap.SetStart(t);
    }

    private void AddToTargetGroup()
    {
        TargetGroup.AddMember(CurSelectedPlayer, PlayerTargetGroupWeight, PlayerTargetGroupRadius);
    }

    private void DisablePlayer()
    {
        foreach (var k in Players)
        {
            var t = CurSelectedPlayer.GetComponent<CharacterHandler>();
            if (k != t)
            {
                k.gameObject.SetActive(false);
            }
        }
    }

    private bool OnClickStart()
    {
        return CrossPlatformInputManager.GetButtonDown("GameStart");
    }

    private void DetectSwipe()
    {
        if (SwipeLeft())
        {
            NextCharacter(true);
            SetStartCamTarget();
        }
        else if (SwipeRight())
        {
            NextCharacter(false);
            SetStartCamTarget();
        }
    }

    private void SetStartCamTarget()
    {
        StartCamera.Follow = CurSelectedPlayer.transform;
        StartCamera.LookAt = CurSelectedPlayer.transform;
    }

    private void SetCinematicCamTarget(Transform t)
    {
        CinematicCamera.Follow = t;
        CinematicCamera.LookAt = t;
    }

    private void NextCharacter(bool reverse)
    {
        if (!reverse)
        {
            var index = CurIndex < Players.Count - 1 ? Mathf.Clamp(CurIndex + 1, 0, Players.Count) : 0;
            CurSelectedPlayer = Players[index].transform;
            CurIndex = index;
        }
        else if (reverse)
        {
            var index = CurIndex > 0 ? Mathf.Clamp(CurIndex - 1, 0, Players.Count - 1) : Players.Count - 1;
            CurSelectedPlayer = Players[index].transform;
            CurIndex = index;
        }
    }

    private bool SwipeRight()
    {
        var r = CrossPlatformInputManager.GetButtonDown("ArrowRight");
        return r;
    }

    private bool SwipeLeft()
    {
        var l = CrossPlatformInputManager.GetButtonDown("ArrowLeft");
        return l;
    }

    private void FindPlayers()
    {
        Players = new List<CharacterHandler>();
        var g = GameObject.FindGameObjectsWithTag("Player");
        foreach (var obj in g)
        {
            var icha = obj.GetComponent<CharacterHandler>();

            if (!Players.Contains(icha))
            {
                Players.Add(icha);
            }
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
