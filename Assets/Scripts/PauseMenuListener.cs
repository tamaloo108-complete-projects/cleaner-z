﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using TMPro;

public class PauseMenuListener : MonoBehaviour
{
    [SerializeField] GameObject PauseMenu;
    [SerializeField] GameObject ControllerHUD;
    [SerializeField] GameObject GeneralHUD;
    [SerializeField] GameObject MainMenuHUD;
    [SerializeField] GameObject FinishHUD;
    [SerializeField] GameObject Effect1HUD;
    [SerializeField] GameObject Effect2HUD;
    [SerializeField] Animator FinishAnim;

    [Header("Scores")]
    [SerializeField] TextMeshProUGUI MobScores;
    [SerializeField] TextMeshProUGUI BossScores;
    [SerializeField] TextMeshProUGUI TotalScores;

    float TimeToEnd = 3.3f;
    float TimeToEndCounter = 0f;

    MusicPlayer mPlayer;
    // Start is called before the first frame update
    void Start()
    {
        TimeToEndCounter = TimeToEnd;
        mPlayer = GameObject.FindGameObjectWithTag("Sound Manager").GetComponent<MusicPlayer>();
        PauseMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Pause"))
        {
            DoPause();
        }

        if (CrossPlatformInputManager.GetButtonDown("Resume"))
        {
            ResumeGame();
        }

        if (CrossPlatformInputManager.GetButtonDown("Retry"))
        {
            RetryLevel();
        }

        if (CrossPlatformInputManager.GetButtonDown("Main Menu"))
        {
            MainMenu();
        }

        if (MixedHandler.Instance.GameIsOver)
        {
            TimeToEndCounter -= Time.deltaTime;


            if (!FinishAnim.GetBool("Active"))
                FinishGame();

        }
    }

    private void FinishGame()
    {
        Effect1HUD.SetActive(false);
        Effect2HUD.SetActive(false);
        ControllerHUD.SetActive(false);
        GeneralHUD.SetActive(false);
        MainMenuHUD.SetActive(false);
        if (TimeToEndCounter > .1f) return;
        mPlayer.Stop("MusicSources");
        if (MixedHandler.Instance.WeWon)
            mPlayer.Play("Victory", "MusicSources");
        else
            mPlayer.Play("Lose", "MusicSources");

        FinishHUD.SetActive(true);
        FinishAnim.SetBool("Active", true);
        CalculateScore();
    }

    private void CalculateScore()
    {
        MobScores.SetText(MixedHandler.Instance.MobScore.ToString());
        BossScores.SetText(MixedHandler.Instance.BossScore.ToString());
        TotalScores.SetText((MixedHandler.Instance.MobScore + MixedHandler.Instance.BossScore).ToString());
    }

    private void MainMenu()
    {
        MixedHandler.Instance.WeWon = false;
        MixedHandler.Instance.GameOnPause = false;
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync("Title");
    }

    private void RetryLevel()
    {
        MixedHandler.Instance.WeWon = false;
        MixedHandler.Instance.GameOnPause = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");
    }

    private void ResumeGame()
    {
        MixedHandler.Instance.GameOnPause = false;
        MixedHandler.Instance.DoSpawnEnemy = true;
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    private void DoPause()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0f;
        MixedHandler.Instance.GameOnPause = true;
        MixedHandler.Instance.DoSpawnEnemy = false;
    }
}
