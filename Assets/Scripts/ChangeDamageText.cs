﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ChangeDamageText : MonoBehaviour
{
    public TextMeshProUGUI _TextMesh;
    public Vector3 Offset;

    public void SetPosition(Vector3 g)
    {
        //transform.localPosition = Vector3.zero;
        transform.localPosition += Offset;
    }

    public void SetTextDMG(string t)
    {
        _TextMesh.text = t;
        Destroy(gameObject, 0.9f);
    }
}
