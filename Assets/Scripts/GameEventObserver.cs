﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventObserver : MonoBehaviour
{
    public static PowerUpSpawnPreferences SpawnPreferences;
    public delegate void OnSpawn(Vector3 position);
    public static Vector3 _GetPosition;
    public static event OnSpawn OnSpawnPowerUp;

    public static bool DoSpawnPowerUp;

    private void Update()
    {
        if (DoSpawnPowerUp)
        {
            DoSpawnPowerUp = false;
            OnSpawnPowerUp.Invoke(_GetPosition);
            _GetPosition = Vector3.zero;
        }
    }
}
