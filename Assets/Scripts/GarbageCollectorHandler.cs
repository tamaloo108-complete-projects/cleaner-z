﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;

public class GarbageCollectorHandler : MonoBehaviour
{
    private void Start()
    {
//#if !UNITY_EDITOR
//                if (Application.isPlaying)
//                {
//                    DisableGC();
//                }
//#endif
    }

    private void Update()
    {
//#if !UNITY_EDITOR
//        if (Time.frameCount % 10 == 0)
//        {
//           EnableGC();
//        }
//        else
//        {
//            DisableGC();
//        }
//#endif
    }

    static void ListenForGCModeChange()
    {
        // Listen on garbage collector mode changes.
        GarbageCollector.GCModeChanged += (GarbageCollector.Mode mode) =>
        {
            Debug.Log("GCModeChanged: " + mode);
        };
    }

    static void LogMode()
    {
        Debug.Log("GCMode: " + GarbageCollector.GCMode);
    }

    static void EnableGC()
    {
        GarbageCollector.GCMode = GarbageCollector.Mode.Enabled;
        // Trigger a collection to free memory.
        System.GC.Collect();
    }

    static void DisableGC()
    {
        GarbageCollector.GCMode = GarbageCollector.Mode.Disabled;
    }
}
