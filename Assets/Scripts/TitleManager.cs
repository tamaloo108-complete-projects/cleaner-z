﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour
{


    public void StartGame()
    {
        MixedHandler.Instance.MobScore = 0;
        MixedHandler.Instance.BossScore = 0;
        MixedHandler.Instance.WeWon = false;
        MixedHandler.Instance.GameIsOver = false;
        MixedHandler.Instance.DoSpawnEnemy = false;
        MixedHandler.Instance.AllFreeze = false;
        MixedHandler.Instance.GameOnPause = false;
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }
    public void RetryGame()
    {
        MixedHandler.Instance.MobScore = 0;
        MixedHandler.Instance.BossScore = 0;
        MixedHandler.Instance.WeWon = false;
        MixedHandler.Instance.GameIsOver = false;
        MixedHandler.Instance.DoSpawnEnemy = false;
        MixedHandler.Instance.AllFreeze = false;
        MixedHandler.Instance.GameOnPause = false;
        SceneManager.LoadScene("Game");
    }

    public void TitleScreen()
    {
        MixedHandler.Instance.MobScore = 0;
        MixedHandler.Instance.BossScore = 0;
        MixedHandler.Instance.WeWon = false;
        MixedHandler.Instance.GameIsOver = false;
        MixedHandler.Instance.DoSpawnEnemy = false;
        MixedHandler.Instance.AllFreeze = false;
        MixedHandler.Instance.GameOnPause = false;
        SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ShowHelp()
    {
        //todo;
    }
}
