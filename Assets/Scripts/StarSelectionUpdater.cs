﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StarSelectionUpdater : MonoBehaviour
{
    [Header("Power")]
    [SerializeField] GameObject pwr;
    List<GameObject> _pwr = new List<GameObject>();

    [Header("Speed")]
    [SerializeField] GameObject spd;
    List<GameObject> _spd = new List<GameObject>();

    [Header("Growth")]
    [SerializeField] GameObject grow;
    List<GameObject> _grow = new List<GameObject>();

    [SerializeField]
    TextMeshProUGUI NameText;

    [SerializeField] CameraSetter _CS;

    CharacterHandler curPlayer;
    // Start is called before the first frame update
    void Start()
    {
        GetPwr();
        GetSpd();
        GetGrow();
        UpdateName();
        //curPlayer = _CS.GetCurrentPlayer();

        UpdateStar();
    }

    private void UpdateName()
    {
        var k = _CS.GetCurrentPlayer().myInitData.name;
        NameText.SetText(k);
    }

    private void GetGrow()
    {
        var child = grow.GetComponentsInChildren<Transform>();

        foreach (Transform _g in grow.transform)
        {
            if (_g != grow)
            {
                _grow.Add(_g.gameObject);
            }
        }
    }

    private void GetSpd()
    {

        var child = spd.GetComponentsInChildren<Transform>();

        foreach (Transform _g in spd.transform)
        {
            if (_g != spd)
            {
                _spd.Add(_g.gameObject);
            }
        }
    }

    private void GetPwr()
    {

        var child = pwr.GetComponentsInChildren<Transform>();

        foreach (Transform _g in pwr.transform)
        {
            if (_g != pwr)
            {
                _pwr.Add(_g.gameObject);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdateName();
        UpdateStar();
    }

    private void UpdateStar()
    {
        var g = _CS.GetCurrentPlayer().myInitData.GrowRating;
        var s = _CS.GetCurrentPlayer().myInitData.SpeedRating;
        var p = _CS.GetCurrentPlayer().myInitData.PowerRating;


        switch (g)
        {
            case 0:
                _grow[0].SetActive(false);
                _grow[1].SetActive(false);
                _grow[2].SetActive(false);
                break;

            case 1:
                _grow[0].SetActive(true);
                _grow[1].SetActive(false);
                _grow[2].SetActive(false);
                break;

            case 2:
                _grow[0].SetActive(true);
                _grow[1].SetActive(true);
                _grow[2].SetActive(false);
                break;

            default:
                _grow[0].SetActive(true);
                _grow[1].SetActive(true);
                _grow[2].SetActive(true);
                break;
        }

        switch (s)
        {
            case 0:
                _spd[0].SetActive(false);
                _spd[1].SetActive(false);
                _spd[2].SetActive(false);
                break;

            case 1:
                _spd[0].SetActive(true);
                _spd[1].SetActive(false);
                _spd[2].SetActive(false);
                break;

            case 2:
                _spd[0].SetActive(true);
                _spd[1].SetActive(true);
                _spd[2].SetActive(false);
                break;

            default:
                _spd[0].SetActive(true);
                _spd[1].SetActive(true);
                _spd[2].SetActive(true);
                break;
        }

        switch (p)
        {
            case 0:
                _pwr[0].SetActive(false);
                _pwr[1].SetActive(false);
                _pwr[2].SetActive(false);
                break;

            case 1:
                _pwr[0].SetActive(true);
                _pwr[1].SetActive(false);
                _pwr[2].SetActive(false);
                break;

            case 2:
                _pwr[0].SetActive(true);
                _pwr[1].SetActive(true);
                _pwr[2].SetActive(false);
                break;

            default:
                _pwr[0].SetActive(true);
                _pwr[1].SetActive(true);
                _pwr[2].SetActive(true);
                break;
        }

    }
}
