﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : BaseState
{
    private GameObject g;
    private Rigidbody myRb;
    private Animator myAnim;

    private GameObject _target;
    private Vector3 _targetPosition;


    public IdleState(GameObject g) : base(g)
    {
        this.g = g;
        myRb = g.GetComponent<Rigidbody>();
        myAnim = g.GetComponent<Animator>();
        _target = g.GetComponent<BossBehaviour>().Target;
        _targetPosition = _target.transform.position;
    }

    public override Type Think()
    {
        #region Handle Rotation
        //var dir = (_target.transform.position - g.transform.position).normalized;
        //var rot = Quaternion.Lerp(myRb.rotation, Quaternion.LookRotation(dir), Time.deltaTime * 10f);
        //myRb.MoveRotation(rot);
        #endregion


        Debug.Log("do nothing");

        return typeof(MoveState);
    }
}
