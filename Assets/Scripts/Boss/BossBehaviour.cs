﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BossBehaviour : MonoBehaviour
{
    [SerializeField] BossTypes _BossBehaviorState = BossTypes.Zombie;
    BossMoveTypes _BossMovement = BossMoveTypes.Teleport;
    BossAttackTypes _BossAttack = BossAttackTypes.None;
    [SerializeField] Animator myLaserAnim;
    Rigidbody MyRb;
    Animator myAnim;
    

    public GameObject Target;
    Dictionary<Type, BaseState> states;
    public BaseState CurrentState { get; private set; }
    public event Action<BaseState> OnStateChanged;

    Coroutine BehaviorCoroutine;

    private void Awake()
    {
        MyRb = GetComponent<Rigidbody>();
        myAnim = GetComponent<Animator>();
        Target = GameObject.FindGameObjectWithTag("Player");
        InitBehavior();
    }

    private void Start()
    {
        //if (BehaviorCoroutine == null)
        //{
        //    BehaviorCoroutine = StartCoroutine(MainFrame());
        //}
    }

    private void FixedUpdate()
    {
        if (MixedHandler.Instance.AllFreeze || MixedHandler.Instance.GameOnPause || MixedHandler.Instance.GameIsOver) return;

        if (CurrentState == null)
        {
            CurrentState = states.Values.First();
        }

        var nextState = CurrentState?.Think();

        if (nextState != null && nextState != CurrentState?.GetType())
        {
            SwitchToState(nextState);
        }

    }

    private void InitBehavior()
    {
        switch (_BossBehaviorState)
        {
            case BossTypes.Carrier:
                _BossMovement = BossMoveTypes.Hover;
                _BossAttack = BossAttackTypes.Bullet;
                break;

            case BossTypes.Jumper:
                _BossMovement = BossMoveTypes.Jump;
                _BossAttack = BossAttackTypes.None;
                break;

            case BossTypes.Wrecker:
                _BossMovement = BossMoveTypes.Rush;
                _BossAttack = BossAttackTypes.None;
                break;

            case BossTypes.Havoc:
                _BossMovement = BossMoveTypes.Hover;
                _BossAttack = BossAttackTypes.Beam;
                break;

            default:
                _BossMovement = BossMoveTypes.Hover;
                _BossAttack = BossAttackTypes.None;
                break;
        }

        var _states = new Dictionary<Type, BaseState>
                {
                    { typeof(IdleState), new IdleState(this.gameObject)},
                    { typeof(MoveState), new MoveState(this.gameObject, _BossMovement)},
                    { typeof(AttackState), new AttackState(this.gameObject, _BossAttack, myLaserAnim)}
                };

        states = _states;
    }

    private IEnumerator MainFrame()
    {
        while (true)
        {
            yield return new WaitUntil(() => !MixedHandler.Instance.AllFreeze && !MixedHandler.Instance.GameOnPause && !MixedHandler.Instance.GameIsOver);

            if (CurrentState == null)
            {
                CurrentState = states.Values.First();
            }

            var nextState = CurrentState?.Think();

            if (nextState != null && nextState != CurrentState?.GetType())
            {
                SwitchToState(nextState);
            }

            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

    private void SwitchToState(Type nextState)
    {
        CurrentState = states[nextState];
        OnStateChanged?.Invoke(CurrentState);
    }
}

public enum BossTypes
{
    Zombie = 0,
    Carrier = 1,
    Jumper = 2,
    Wrecker = 3,
    Havoc = 4

}

public enum BossMoveTypes
{
    Teleport = 0,
    Jump = 1,
    Hover = 2,
    Rush = 3
}

public enum BossAttackTypes
{
    None = 0,
    Beam = 1,
    Bullet = 2,
    ShockWave = 3
}