﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : BaseState
{
    private GameObject _g;
    private Rigidbody myRb;
    private Animator myAnim;
    private Animator myLaserAnim;


    private GameObject _target;
    private Vector3 _targetPosition;

    bool onBeam = false;
    float beamDur = .2f;
    float beamDurCount = 0f;


    private BossAttackTypes _BossAttack;
    public AttackState(GameObject g) : base(g)
    {
        _g = g;
        myRb = g.GetComponent<Rigidbody>();
        myAnim = g.GetComponent<Animator>();
        _target = g.GetComponent<BossBehaviour>().Target;
        _targetPosition = _target.transform.position;
        beamDurCount = beamDur;
    }

    public AttackState(GameObject g, BossAttackTypes AttackTypes, Animator myLaserAnim) : base(g)
    {
        _g = g;
        myRb = g.GetComponent<Rigidbody>();
        myAnim = g.GetComponent<Animator>();
        _target = g.GetComponent<BossBehaviour>().Target;
        _targetPosition = _target.transform.position;
        _BossAttack = AttackTypes;
        this.myLaserAnim = myLaserAnim;
        beamDurCount = beamDur;
    }

    public override Type Think()
    {
        beamDurCount -= .1f;
        Debug.Log(beamDurCount);
        switch (_BossAttack)
        {
            case BossAttackTypes.Beam:
                if (!onBeam && beamDurCount <= -0.03f)
                {
                    myAnim.SetBool("Beam", true);
                }

                if (!onBeam && beamDurCount <= -0.1f)
                {
                    myLaserAnim.SetBool("Fire", true);
                    onBeam = true;
                }
                else if (onBeam && beamDurCount <= 0f)
                {
                    beamDurCount = beamDur;
                    myLaserAnim.SetBool("Fire", false);
                    myAnim.SetBool("Beam", false);
                    onBeam = false;
                }
                return typeof(IdleState);

            case BossAttackTypes.None:
                return typeof(IdleState);
        }

        return null;
    }
}
