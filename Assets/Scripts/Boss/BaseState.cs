﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseState
{
    public BaseState(GameObject g)
    {
        _gameObject = g;
        _transform = g.transform;
    }

    public BaseState(GameObject g, BossMoveTypes BossMovement)
    {
        _gameObject = g;
        _transform = g.transform;
    }

    protected GameObject _gameObject;
    protected Transform _transform;

    public abstract Type Think();

}
