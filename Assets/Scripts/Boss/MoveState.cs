﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState : BaseState
{
    private GameObject _g;
    private Rigidbody myRb;
    private Animator myAnim;

    private GameObject _target;
    private Vector3 _targetPosition;

    float _moveSpeed = 5.3f;
    bool isFindTarget = false;
    bool onJumpState = false;
    bool onAnimState = true;
    float _idleTime = 1.5f;
    float _moveDelayTime = .4f;
    float _countTime = 0f;
    float _countDelayTime = 0f;
    Vector3 _velocity = Vector3.zero;

    BossMoveTypes _BossMove;
    private bool DoJump;

    Coroutine JumpBehaviorCoroutine;

    public MoveState(GameObject g, BossMoveTypes BossMove) : base(g)
    {
        _g = g;
        myRb = g.GetComponent<Rigidbody>();
        myAnim = g.GetComponent<Animator>();
        _target = g.GetComponent<BossBehaviour>().Target;
        _targetPosition = _target.transform.position;
        _countTime = _idleTime;
        _countDelayTime = _moveDelayTime;
        _BossMove = BossMove;
    }

    public override Type Think()
    {
        #region Handle Rotation
        var dir = (_target.transform.position - _g.transform.position).normalized;
        var rot = Quaternion.Lerp(_g.transform.rotation, Quaternion.LookRotation(dir), 6f * Time.fixedDeltaTime);
        myRb.MoveRotation(rot);
        #endregion

        Debug.Log("Delay :" + _countDelayTime);
        Debug.Log("Time : " + _countTime);

        if (!isFindTarget)
        {
            _countTime = _idleTime;
            isFindTarget = true;
        }
        else

        if (isFindTarget && _countTime <= 0f)
        {
            isFindTarget = false;
            _targetPosition = _target.transform.position;
            DoJump = false;
            // _countDelayTime = _moveDelayTime;
        }

        _countTime -= Time.deltaTime; //delay to find target.
        _countDelayTime -= Time.deltaTime; //delay to move to target.

        #region Movement
        if (MixedHandler.Instance.BossEntrance) return typeof(IdleState);

        switch (_BossMove)
        {
            case BossMoveTypes.Teleport:
                if (_countDelayTime <= 0f)
                {
                    _g.transform.position = _targetPosition;
                    myRb.MovePosition(_targetPosition);

                }
                else if (_countDelayTime <= -1f)
                {
                    _countDelayTime = _moveDelayTime;
                    return typeof(AttackState);
                }
                return typeof(IdleState);

            case BossMoveTypes.Hover:
                if (_countDelayTime > 0f)
                {
                    if (myAnim.GetBool("Beam")) return typeof(IdleState);
                    //var k = Vector3.Lerp(_g.transform.position, _targetPosition, 2f * Time.fixedDeltaTime);
                    //_g.transform.position = Vector3.MoveTowards(_g.transform.position, k, _moveSpeed);
                    myRb.isKinematic = true;
                    var lerp = Vector3.SmoothDamp(_g.transform.position, _target.transform.position, ref _velocity, 1f);
                    myRb.MovePosition(lerp);
                    //myRb.AddForce(k * _moveSpeed);


                    //simply move to target.
                }
                else if (_countDelayTime <= -1f)
                {
                    myRb.isKinematic = false;
                    _countDelayTime = _moveDelayTime;
                    return typeof(AttackState);
                }
                return typeof(IdleState);

            case BossMoveTypes.Rush:
                //   if (_countDelayTime > 0f)
                //  {
                myAnim.SetBool("Dash", myRb.velocity.magnitude > 0f);
                //  }

                if (_countDelayTime > 0f)
                {
                    onAnimState = true;
                    var newPos = _target.transform.position + (_g.transform.forward * 30f);
                    var dest = Vector3.Lerp(_g.transform.position, newPos, Time.fixedDeltaTime).normalized;
                    myRb.velocity = _moveSpeed * _g.transform.forward;
                    //simply move to target.
                }
                else if (_countDelayTime <= -1f)
                {
                    myAnim.SetBool("Dash", false);
                    onAnimState = false;
                    _countDelayTime = _moveDelayTime;
                    return typeof(AttackState);
                }

                return typeof(IdleState);

            case BossMoveTypes.Jump:
                if (_countDelayTime > 0f)
                {
                    if (DoJump)
                    {
                        var NewTargetPos = new Vector3(_target.transform.position.x, _g.transform.position.y, _target.transform.position.z);
                        var lerp = Vector3.Lerp(myRb.position, NewTargetPos, Time.fixedDeltaTime);
                        var _smooth = Vector3.SmoothDamp(lerp, NewTargetPos, ref _velocity, 1f, 5f);
                        myRb.AddForce(100f * dir, ForceMode.Acceleration);
                    }
                }
                else

                if (_countDelayTime <= 0f)
                {
                    if (onJumpState)
                    {
                        myAnim.SetBool("Jump", false);
                        onJumpState = false;
                    }

                    if (!DoJump)
                    {
                        myAnim.SetBool("Jump", true);
                        myRb.AddForce(_g.transform.up * 400f, ForceMode.Impulse);
                        //this one requires to simulate the jump.
                        DoJump = true;
                        onJumpState = true;
                        _countDelayTime = _moveDelayTime;
                    }


                }
                return typeof(AttackState);

        }

        #endregion
        return null;

    }

    private IEnumerator BehaveJump()
    {
        while (true)
        {
            if (onJumpState)
            {
                _targetPosition.y = 0f;
                myRb.MovePosition(_targetPosition);
                myRb.isKinematic = false;
                myRb.AddForce(-_g.transform.up * 100f, ForceMode.Impulse);
                onJumpState = false;
                DoJump = false;
                _countDelayTime = _moveDelayTime;
                JumpBehaviorCoroutine = null;
                yield break;
            }

            if (!DoJump)
            {
                myRb.AddForce(_g.transform.up * 500f, ForceMode.Impulse);
                //this one requires to simulate the jump.
                DoJump = true;
                onJumpState = true;
                myRb.isKinematic = true;
                _countDelayTime = _moveDelayTime;
                JumpBehaviorCoroutine = null;
                yield break;
            }
            yield return new WaitForSeconds(0.01f);
        }
    }
}
