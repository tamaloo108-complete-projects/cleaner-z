﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SpawnerData", menuName = "Cleaner-Z/Create New Spawn List")]
public class SpawnerData : ScriptableObject
{
    [SerializeField] private List<SpawnerWave> _Wave;
    [SerializeField] private bool _Endless;

    public List<SpawnerWave> Wave { get => _Wave; }
    public bool Endless { get => _Endless; }
}

[System.Serializable]
public class SpawnerWave
{
    public List<int> _MobIndex;
    public int _MobWaveCount; //mob count on this wave
    public bool _BossBattle;
    public List<int> _BossIndex;
    [Range(0, 4)] public int MaxBossOnScreen = 0;
    [Range(0f, 10f)] public float BossTimer = 1f;
    [Range(0.1f, 10f)] public float _TimeDelay = 1f;
}