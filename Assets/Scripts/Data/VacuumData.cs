﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCharacterVacuum", menuName = "Cleaner-Z/Create Character Vacuum")]
public class VacuumData : ScriptableObject
{
    [SerializeField] [Range(0.1f, 20f)] float _VacuumPower = 0.5f;
    [SerializeField] [Range(1, 25)] int _VacuumRange = 2;

    public float VacuumPower { get => _VacuumPower; }
    public float VacuumRange { get => _VacuumRange; }
}
