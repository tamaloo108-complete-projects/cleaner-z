﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "VirusTankData", menuName = "Cleaner-Z/Create New Virus Tank")]
public class VirusTankData : ScriptableObject
{
    [SerializeField] [Range(1f,9999f)] float _durability = 15f;
    [SerializeField] [Range(10f, 9999f)] float _timer = 120f;
    [SerializeField] [Range(10f, 9999f)] float _normalMobScore = 20f;
    [SerializeField] [Range(10f, 9999f)] float _bigMobScore = 250f;
    [SerializeField] [Range(10f, 9999f)] float _bossMobScore = 1000f;

    public float Durability { get => _durability; }
    public float Timer { get => _timer; }
    public float NormalMobScore { get => _normalMobScore; }
    public float BigMobScore { get => _bigMobScore; }
    public float BossMobScore { get => _bossMobScore; }
}
