﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewGameSetting", menuName = "Cleaner-Z/Create New Game Settings")]
public class GameSettingsData : ScriptableObject
{

    public enum QualityType
    {
        Low = 0,
        Medium = 1,
        High = 2
    }

    [SerializeField] QualityType _QualitySet = QualityType.Medium;
    [SerializeField] bool _Bloom = false;
    [SerializeField] bool _PostProcessing = true;
    [SerializeField] [Range(0.0001f, 1f)] float _MasterVolume = 1f;
    [SerializeField] [Range(0.0001f, 1f)] float _SFXVolume = 1f;
    [SerializeField] [Range(0.0001f, 1f)] float _BGMVolume = 1f;

    public QualityType QualitySet { get => _QualitySet; set => _QualitySet = value; }
    public bool Bloom { get => _Bloom; set => _Bloom = value; }
    public float MasterVolume { get => _MasterVolume; set => _MasterVolume = value; }
    public float SFXVolume { get => _SFXVolume; set => _SFXVolume = value; }
    public bool PostProcessing { get => _PostProcessing; set => _PostProcessing = value; }
    public float BGMVolume { get => _BGMVolume; set => _BGMVolume = value; }
}