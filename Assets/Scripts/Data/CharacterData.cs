﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewCharacterData", menuName = "Cleaner-Z/Create New Character")]
public class CharacterData : ScriptableObject
{
    [SerializeField] CharacterTypes _CharacterType = CharacterTypes.Mob;
    [SerializeField] [Range(0f, 9999f)] float _CharacterHealth = 0f;
    [SerializeField] [Range(0f, 500f)] float _CharacterAttack = 0f;

    [SerializeField] [Range(0f, 10f)] float _DeliverDamageTimer = 0f; //Mob Exclusive,Nah.
    [SerializeField] [Range(0.1f, 10f)] float _DownDuration = 0.5f;
    [SerializeField] [Range(30f, 100f)] float _KnockbackForce = 30f;
    [SerializeField] [Range(30f, 100f)] float _KnockUpForce = 30f;
    [SerializeField] [Range(10f, 1000f)] float _MobScore = 15f;

    [Header("Player Exclusive")]
    [SerializeField] [Range(0, 3)] int _PowerRating = 1;
    [SerializeField] [Range(0, 3)] int _SpeedRating = 1;
    [SerializeField] [Range(0, 3)] int _GrowRating = 1;

    public float CharacterHealth { get => _CharacterHealth; }
    public float CharacterAttack { get => _CharacterAttack; }
    public float DeliverDamageTimer { get => _DeliverDamageTimer; }
    public float DownDuration { get => _DownDuration; }
    public float KnockbackForce { get => _KnockbackForce; }
    public float KnockUpForce { get => _KnockUpForce; }
    public float MobScore { get => _MobScore; }
    public int PowerRating { get => _PowerRating; }
    public int SpeedRating { get => _SpeedRating; }
    public int GrowRating { get => _GrowRating; }
    internal CharacterTypes CharacterType { get => _CharacterType; }
}

enum CharacterTypes
{
    Player = 0, Mob = 1, Boss = 2
}
