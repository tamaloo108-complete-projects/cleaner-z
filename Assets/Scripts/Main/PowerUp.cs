﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] PowerUpTypes PowerUpPreference;
    MusicPlayer mPlayer;
    Rigidbody myRb;
    public EnemySpawner OnPlayerCollidePowerUp;
    bool Picked = false;
    bool _SetAnimated = false;

    private void OnEnable()
    {
        myRb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        mPlayer = GameObject.FindGameObjectWithTag("Sound Manager").GetComponent<MusicPlayer>();
        StartCoroutine(EnablePickUp());
    }


    private IEnumerator EnablePickUp()
    {
        yield return new WaitForSeconds(0.35f);
        _SetAnimated = false;
    }

    public void EmulateExplosionForce()
    {
        myRb.AddExplosionForce(50f, transform.position, 1f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Picked) return;
        if (_SetAnimated) return;
        var col = other.gameObject;
        if (other.gameObject.tag == "Player")
        {
            Picked = true;
            mPlayer.Play("Power Up1", "SFXSources");
            mPlayer.PlayScheduled("Power Up2", "PlayerSFXSources", 0.45f);

            var PuHolder = col.GetComponent<PowerUpHolder>();
            PuHolder.AddPowerUp(PowerUpPreference);
            if (PowerUpPreference == PowerUpTypes.RangeUp || PowerUpPreference == PowerUpTypes.PowerUp)
                MixedHandler.Instance.onUpgradeVacuum = true;

            //if (PowerUpPreference != PowerUpTypes.Heal)
            OnPlayerCollidePowerUp.SpawnParticle(PowerUpPreference, col.transform);
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (Picked) return;
        //if (_SetAnimated) return;
        //var col = collision.gameObject;
        //if (collision.gameObject.tag == "Player")
        //{
        //    Picked = true;
        //    mPlayer.Play("Power Up1", "SFXSources");
        //    mPlayer.PlayScheduled("Power Up2", "PlayerSFXSources", 0.45f);

        //    var PuHolder = col.GetComponent<PowerUpHolder>();
        //    PuHolder.AddPowerUp(PowerUpPreference);
        //    if (PowerUpPreference == PowerUpTypes.RangeUp || PowerUpPreference == PowerUpTypes.PowerUp)
        //        MixedHandler.Instance.onUpgradeVacuum = true;

        //    //if (PowerUpPreference != PowerUpTypes.Heal)
        //    OnPlayerCollidePowerUp.SpawnParticle(PowerUpPreference, col.transform);
        //    Destroy(gameObject);
        //}
    }

    //private void OnDestroy()
    //{

    //}

    internal void SetAnimated(bool v)
    {
        _SetAnimated = v;
    }
}

public enum PowerUpTypes
{
    Heal = 0, AttackUp = 1, SpeedUp = 2, PowerUp = 3, RangeUp = 4
}
