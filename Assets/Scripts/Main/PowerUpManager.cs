﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class PowerUpManager : MonoBehaviour
{
    Vector3 CurrentSpawnPosition;
    [SerializeField] Vector3 SpawnOffset;
    [SerializeField] PowerUpSpawnPreferences SpawnPreferences;
    [SerializeField] Collider SpawnArea;
    [SerializeField] int MaximumPowerUpOnScene = 3;

    [SerializeField] AnimationCurve SpawnProbabilitySeed;
    [SerializeField] [Range(0.01f, 1f)] float Chances = .3f;

    [SerializeField] private List<PowerUps> _PowerUpRefences = new List<PowerUps>();

    private readonly Dictionary<AssetReference, List<GameObject>> _SpawnedPowerUp = new Dictionary<AssetReference, List<GameObject>>();
    private readonly Dictionary<AssetReference, Queue<Vector3>> _queueSpawnReq = new Dictionary<AssetReference, Queue<Vector3>>();
    private readonly Dictionary<AssetReference, AsyncOperationHandle<GameObject>> _asyncOperationHandle = new Dictionary<AssetReference, AsyncOperationHandle<GameObject>>();

    EnemySpawner _EnemySpawner;

    private void Start()
    {
        GameEventObserver.SpawnPreferences = SpawnPreferences;
        GameEventObserver.OnSpawnPowerUp += GameEventObserver_OnSpawnPowerUp;
        _EnemySpawner = GetComponent<EnemySpawner>();
    }

    private void GameEventObserver_OnSpawnPowerUp(Vector3 position)
    {
        int index = RandomizeSpawn();
        float val = ProbableSpawn();
        if (val < Chances)
        {
            CurrentSpawnPosition = position + SpawnOffset;
            StartCoroutine(Spawn(index));
        }
    }


    private void OnDisable()
    {
        GameEventObserver.OnSpawnPowerUp -= GameEventObserver_OnSpawnPowerUp;
    }

    private float ProbableSpawn()
    {
        return SpawnProbabilitySeed.Evaluate(UnityEngine.Random.value);
    }

    private int RandomizeSpawn()
    {
        return UnityEngine.Random.Range(0, _PowerUpRefences.Count);
    }

    IEnumerator Spawn(int index)
    {
        if (index < 0 || index > _PowerUpRefences.Count)
            yield break;

        AssetReference _assetreference = _PowerUpRefences[index].Reference;

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            yield break;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
            {
                SpawnFromLoaderReference(_assetreference, CurrentSpawnPosition);
            }
            else
                EnqueueSpawnForAfterInitialization(_assetreference, CurrentSpawnPosition);
            yield break;
        }

        LoadAndSpawn(_assetreference, CurrentSpawnPosition);

        yield break;
    }

    public void Spawn(PowerUpTypes types, Transform pos, bool Animated)
    {

        AssetReference _assetreference = _PowerUpRefences.Find(x => x.Types.Equals(types)).Reference;

        if (_assetreference.RuntimeKeyIsValid() == false)
        {
            Debug.Log("Invalid Key" + _assetreference.RuntimeKey);
            return;
        }

        if (_asyncOperationHandle.ContainsKey(_assetreference))
        {
            if (_asyncOperationHandle[_assetreference].IsDone)
            {
                SpawnFromLoaderReference(_assetreference, pos.position, Animated);
            }
            else
                EnqueueSpawnForAfterInitialization(_assetreference, pos.position, Animated);
            return;
        }

        LoadAndSpawn(_assetreference, pos.position, Animated);

        return;
    }

    private void LoadAndSpawn(AssetReference assetreference, Vector3 currentSpawnPosition)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnFromLoaderReference(assetreference, currentSpawnPosition);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnFromLoaderReference(assetreference, currentSpawnPosition);
                }
            }
        };
    }

    private void LoadAndSpawn(AssetReference assetreference, Vector3 currentSpawnPosition, bool Animated)
    {
        var op = Addressables.LoadAssetAsync<GameObject>(assetreference);
        _asyncOperationHandle[assetreference] = op;
        op.Completed += operation =>
        {
            SpawnFromLoaderReference(assetreference, currentSpawnPosition, Animated);
            if (_queueSpawnReq.ContainsKey(assetreference))
            {
                while (_queueSpawnReq[assetreference]?.Any() == true)
                {
                    var _pos = _queueSpawnReq[assetreference].Dequeue();
                    SpawnFromLoaderReference(assetreference, currentSpawnPosition, Animated);
                }
            }
        };
    }

    private void EnqueueSpawnForAfterInitialization(AssetReference assetreference, Vector3 currentSpawnPosition)
    {
        if (_queueSpawnReq.ContainsKey(assetreference) == false)
        {
            _queueSpawnReq[assetreference] = new Queue<Vector3>();
        }

        _queueSpawnReq[assetreference].Enqueue(currentSpawnPosition);
    }

    private void EnqueueSpawnForAfterInitialization(AssetReference assetreference, Vector3 currentSpawnPosition, bool Animated)
    {
        if (_queueSpawnReq.ContainsKey(assetreference) == false)
        {
            _queueSpawnReq[assetreference] = new Queue<Vector3>();
        }

        _queueSpawnReq[assetreference].Enqueue(currentSpawnPosition);
    }

    private void SpawnFromLoaderReference(AssetReference assetreference, Vector3 currentSpawnPosition)
    {
        assetreference.InstantiateAsync(currentSpawnPosition, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_SpawnedPowerUp.ContainsKey(assetreference) == false)
            {
                _SpawnedPowerUp[assetreference] = new List<GameObject>();
            }

            _SpawnedPowerUp[assetreference].Add(asyncOperationHandle.Result);
            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            var g = asyncOperationHandle.Result.GetComponent<PowerUp>();
            g.OnPlayerCollidePowerUp = _EnemySpawner;
            notify.Destroyed += Notify_Destroyed;
            notify.AssetReference = assetreference;
            Destroy(g.gameObject, 5f);
        };

    }

    private void SpawnFromLoaderReference(AssetReference assetreference, Vector3 currentSpawnPosition, bool Animated)
    {
        assetreference.InstantiateAsync(currentSpawnPosition, Quaternion.identity).Completed += (asyncOperationHandle) =>
        {
            if (_SpawnedPowerUp.ContainsKey(assetreference) == false)
            {
                _SpawnedPowerUp[assetreference] = new List<GameObject>();
            }

            _SpawnedPowerUp[assetreference].Add(asyncOperationHandle.Result);
            var notify = asyncOperationHandle.Result.AddComponent<NotifyOnDestroy>();
            var g = asyncOperationHandle.Result.GetComponent<PowerUp>();
            g.OnPlayerCollidePowerUp = _EnemySpawner;
            if (Animated)
            {
                g.SetAnimated(true);
                g.EmulateExplosionForce();
                Destroy(g.gameObject, 4.45f);
            }
            else
            {
                Destroy(g.gameObject, 5f);
            }

            notify.Destroyed += Notify_Destroyed;
            notify.AssetReference = assetreference;
        };

    }

    private void Notify_Destroyed(AssetReference assetReference, NotifyOnDestroy obj)
    {
        Addressables.ReleaseInstance(obj.gameObject);

        _SpawnedPowerUp[assetReference].Remove(obj.gameObject);
        if (_SpawnedPowerUp[assetReference].Count == 0)
        {
            if (_asyncOperationHandle[assetReference].IsValid())
            {
                Addressables.Release(_asyncOperationHandle[assetReference]);
            }

            _asyncOperationHandle.Remove(assetReference);
        }

    }
}

[Serializable]
public class PowerUps
{
    [SerializeField] AssetReference _reference;
    [SerializeField] PowerUpTypes _types;

    public AssetReference Reference { get => _reference; }
    public PowerUpTypes Types { get => _types; }
}


public enum PowerUpSpawnPreferences
{
    AfterKill = 0, BeginningOfTheWave = 1, Random = 2
}