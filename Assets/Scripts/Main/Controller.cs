﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.AI;

public interface iController
{
    float GetHorizontal();
    float GetVertical();
    bool GetVacuumButton();
    void CalcSlopeAngleDetection();
    void CalcDirection(float v, float h);
    bool IsIdle(float h, float v);
    void MoveToPosition(Vector3 NewPos);
    void AutoDetectSlope();
    MovementType MovementType { get; }
    void OnVacuumState(bool v);
    void CalcNavigation(Transform target);
    void CalcNavigation(Transform target, CharacterHandler extraTarget);
    void NavmeshInitializing();
    void SimpleInitializing();
    Vector3 CalcTargetDirection();
    void SetTurnRateState(bool val);
    void SendKnockBack(float forceBack, float forceUp);
    void ChangeTargetNavigation();
    void isFreezing();
}

public enum MovementType
{
    NavMesh = 0,
    Simple = 1,
    Controller = 2,
    MixedNavmesh = 3
}

public enum TurnRate
{
    Enable = 0,
    Disable = 1
}


public class Controller : MonoBehaviour, iController
{
    Quaternion LastRotation;
    Vector3 CurrentPosition;
    Rigidbody myRb;
    Camera MainCam;
    NavMeshAgent myAgent;
    Transform myTarget;
    CharacterHandler myExtraTarget;
    Transform myCurrentTarget;
    PowerUpHolder myPowerUp;

    public bool Vacuumed { get; private set; }

    private float angle;
    private bool _grounded;
    private Vector3 _forward;
    private Vector3 groundNormal;
    private float GroundAngle;
    bool onMove = false;
    bool isBoss = false;

    #region Serialized
    [SerializeField] bool isPlayer = false;
    [SerializeField] bool EnableSlopeDetection = true;
    [SerializeField] bool DynamicSlopeDetection = false;
    [SerializeField] private float RayOffset = -.15f;
    [SerializeField] float MovementUpdateInterval = 0.2f;
    [SerializeField] TurnRate TurnRatePreferences = TurnRate.Enable;
    [SerializeField] MovementType MovementPreferences = MovementType.Controller;
    [SerializeField] private float HeightSlopeDetection;
    [SerializeField] private float MaxGroundAngle;
    [SerializeField] private float heightPadding;
    [SerializeField] private LayerMask GroundMask;
    [SerializeField] private float SlopeHeightClimbMult;
    [SerializeField] private float turnSpeed;
    [SerializeField] [Range(0f, 15f)] private float MoveSpeed = .01f;
    [SerializeField] private int maxCollider = 3;

    MovementType iController.MovementType => MovementPreferences;

    #endregion



    private void Start()
    {
        myRb = GetComponent<Rigidbody>();
        if (MainCam == null) MainCam = Camera.main;
        if (isPlayer)
        {
            myPowerUp = GetComponent<PowerUpHolder>();
        }
    }

    float iController.GetHorizontal()
    {
        if (!isPlayer) return 0;
        return CrossPlatformInputManager.GetAxis("Horizontal");
    }

    float iController.GetVertical()
    {
        if (!isPlayer) return 0;
        return CrossPlatformInputManager.GetAxis("Vertical");
    }

    bool iController.GetVacuumButton()
    {
        if (!isPlayer) return false;
        return CrossPlatformInputManager.GetButton("Vacuum");
    }

    void iController.isFreezing()
    {
        if (myAgent != null)
            myAgent.enabled = !MixedHandler.Instance.AllFreeze && !MixedHandler.Instance.BossEntrance;

    }

    void iController.MoveToPosition(Vector3 NewPos)
    {
        _grounded = RayToGround();
        if (!_grounded || Vacuumed) return;

        NewPos *= (MoveSpeed + myPowerUp.bonus.AddedSpeed) * Time.fixedDeltaTime;
        if (EnableSlopeDetection)
        {
            RaycastHit hit;
            if (GroundAngle >= MaxGroundAngle) return;
            Physics.Raycast(transform.position, -Vector3.up, out hit, HeightSlopeDetection + heightPadding, GroundMask);
            if (Vector3.Distance(transform.position, hit.point) < HeightSlopeDetection)
            {
                CurrentPosition = Vector3.Lerp(myRb.position, myRb.position + Vector3.up * HeightSlopeDetection * SlopeHeightClimbMult, Time.fixedDeltaTime);
                if (!isPlayer)
                {
                    myRb.MovePosition(CurrentPosition + NewPos);
                    return;
                }
            }

            if (MovementPreferences == MovementType.Simple)
            {
                NewPos = Vector3.Lerp(CurrentPosition, NewPos, Time.fixedDeltaTime);
                myRb.velocity = transform.forward * (MoveSpeed + myPowerUp.bonus.AddedSpeed);
                return;
            }

            myRb.MovePosition(CurrentPosition + NewPos);
        }
        else
        {
            if (MovementPreferences == MovementType.Simple)
            {
                NewPos = Vector3.Lerp(CurrentPosition, NewPos, Time.fixedDeltaTime);
                myRb.velocity = transform.forward * (MoveSpeed + myPowerUp.bonus.AddedSpeed);
                return;
            }


            myRb.MovePosition(myRb.position + NewPos);
        }


    }

    void iController.CalcDirection(float v, float h)
    {

        angle = Mathf.Atan2(h, v);
        angle = Mathf.Rad2Deg * angle;
        angle += MainCam.transform.eulerAngles.y;

        Rotate(v, h);
    }

    void iController.ChangeTargetNavigation()
    {
        var IsDown = myExtraTarget.isDown;
        var _player = myExtraTarget.transform.position;
        var _tabung = myTarget.transform.position;
        var disToPlayer = Mathf.Abs(Vector3.Distance(transform.position, _player));
        var disToTabung = Mathf.Abs(Vector3.Distance(transform.position, _tabung));

        if (myAgent.enabled)
            myAgent.SetDestination(disToPlayer > disToTabung && IsDown ? _tabung : _player);
    }

    private bool RayToGround()
    {
        var tPos = new Vector3(transform.position.x, transform.position.y - RayOffset, transform.position.z);
        Collider[] lap = new Collider[maxCollider];
        var numCol = Physics.OverlapSphereNonAlloc(tPos, transform.lossyScale.x / 2.4f, lap, GroundMask);

        return numCol >= 1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var tPos = new Vector3(transform.position.x, transform.position.y + 0.22f, transform.position.z);
        Gizmos.DrawWireSphere(tPos, transform.lossyScale.x / 3.9f);

        Gizmos.DrawLine(transform.position, transform.position - Vector3.up * HeightSlopeDetection * heightPadding);
    }

    private Vector3 CalcForward()
    {
        if (!_grounded)
        {
            return transform.forward;
        }

        return Vector3.Cross(groundNormal, transform.right);
    }

    private float CalcGroundAngle(Vector3 groundNormal)
    {
        if (!_grounded)
        {
            return 90f;
        }
        return Vector3.Angle(groundNormal, transform.forward);
    }

    private void Rotate(float v, float h)
    {
        if (Vacuumed) return;
        var dir = new Vector3(h, 0, v);
        if (dir.normalized == Vector3.zero) return;
        Quaternion temp = new Quaternion(0, 0, 0, 0);

        if (TurnRatePreferences == TurnRate.Enable)
            temp = Quaternion.Slerp(myRb.rotation, Quaternion.LookRotation(dir.normalized), turnSpeed * Time.deltaTime);
        else if (TurnRatePreferences == TurnRate.Disable)
            temp = Quaternion.LookRotation(dir.normalized);

        myRb.MoveRotation(temp);
    }

    bool iController.IsIdle(float h, float v)
    {
        return onMove = Mathf.Abs(h) < 1 && Mathf.Abs(v) < 1;
    }

    void iController.CalcSlopeAngleDetection()
    {
        _grounded = RayToGround();
        _forward = CalcForward();
        GroundAngle = CalcGroundAngle(groundNormal);
    }

    void iController.AutoDetectSlope()
    {
        if (!DynamicSlopeDetection) return;
        RaycastHit hit;
        Physics.Raycast(transform.position, -Vector3.up, out hit, HeightSlopeDetection + heightPadding, GroundMask);

        if (Vector3.Distance(transform.position, hit.point) < HeightSlopeDetection)
            EnableSlopeDetection = true;
        else
            EnableSlopeDetection = false;
    }

    void iController.NavmeshInitializing()
    {
        if (myAgent == null)
            myAgent = GetComponent<NavMeshAgent>();

        myAgent.speed = MoveSpeed;
    }

    void iController.CalcNavigation(Transform target)
    {
        myTarget = target;
        myCurrentTarget = myTarget;
        if (myAgent.enabled)
            myAgent.SetDestination(myCurrentTarget.position);

    }

    void iController.CalcNavigation(Transform target, CharacterHandler extraTarget)
    {
        myTarget = target;
        myExtraTarget = extraTarget;
        myCurrentTarget = myTarget;
        isBoss = true;

        if (myAgent.enabled)
            myAgent.SetDestination(myCurrentTarget.position);
    }

    void iController.SimpleInitializing()
    {
        if (myAgent == null)
            myAgent = GetComponent<NavMeshAgent>();

        myAgent.updatePosition = false;
        myAgent.updateRotation = false;
        myAgent.enabled = false;
        myRb.isKinematic = false;
    }

    Vector3 iController.CalcTargetDirection()
    {
        var p = myTarget.position - myRb.position;
        transform.LookAt(p);
        return p;
    }

    void iController.OnVacuumState(bool v)
    {
        if (v)
        {
            if (myAgent != null)
            {
                myAgent.enabled = false;
                myRb.isKinematic = false;
            }
        }
        else
        {
            if (myAgent != null)
            {
                myAgent.enabled = true;
                myRb.isKinematic = true;
            }
        }
    }

    void iController.SetTurnRateState(bool val)
    {
        TurnRatePreferences = val ? TurnRate.Enable : TurnRate.Disable;
    }

    void iController.SendKnockBack(float forceBack, float forceUp)
    {
        var dir = -transform.forward;
        var up = transform.up;
        myRb.AddForce(dir * forceBack, ForceMode.Impulse);
        myRb.AddForce(up * forceUp, ForceMode.Impulse);
    }
}
