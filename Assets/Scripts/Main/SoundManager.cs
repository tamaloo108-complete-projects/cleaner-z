﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : Singleton<SoundManager>
{
    public List<Sounds> MySounds = new List<Sounds>();
    public AudioSource MySources;
    public AudioMixer MyAudioMixer;

    public float minAudioVolume => -80f;
    public float maxAudioVolume => 10f;

    public void SetMasterVolume(float v)
    {
        var val = Convert(v, 0f, 1f, minAudioVolume, maxAudioVolume);
        MyAudioMixer.SetFloat("MasterVolume", Mathf.Log10(v) * 20f);
    }

    public void SetSFXVolume(float v)
    {
        var val = Convert(v, 0f, 1f, minAudioVolume, maxAudioVolume);
        MyAudioMixer.SetFloat("SFX", Mathf.Log10(v) * 20f);
    }

    public void SetBGMVolume(float v)
    {
        MyAudioMixer.SetFloat("BGM", Mathf.Log10(v) * 20f);
    }

    public void Play(string name, AudioSource Sources)
    {
        var index = MySounds.IndexOf(MySounds.Find(x => x._name == name));
        Sources.clip = MySounds[index]._sources;
        Sources.loop = MySounds[index].isLooping;
        Sources.playOnAwake = MySounds[index].PlayOnAwake;
        Sources.volume = MySounds[index].Volume;
        Sources.Play();
    }

    public void PlayScheduled(string name, AudioSource Sources, double delay)
    {
        var index = MySounds.IndexOf(MySounds.Find(x => x._name == name));
        Sources.clip = MySounds[index]._sources;
        Sources.loop = MySounds[index].isLooping;
        Sources.playOnAwake = MySounds[index].PlayOnAwake;
        Sources.volume = MySounds[index].Volume;
        Sources.PlayScheduled(AudioSettings.dspTime + delay);
    }

    public void Play(int index, AudioSource Sources)
    {
        Sources.clip = MySounds[index]._sources;
        Sources.loop = MySounds[index].isLooping;
        Sources.playOnAwake = MySounds[index].PlayOnAwake;
        Sources.volume = MySounds[index].Volume;
        Sources.Play();
    }

    public void Stop(AudioSource Sources)
    {
        Sources.Stop();
    }

    float Convert(float curVal, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (((curVal - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }
}


[System.Serializable]
public class Sounds
{
    public string _name;
    public AudioClip _sources;
    public bool isLooping = false;
    public bool PlayOnAwake = false;
    [Range(0f, 1f)] public float Volume = 1f;
}