﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.Collections;

internal interface iCharacterHandler
{
    void ProcessMove(float v, float h, iController iController);
    CharacterData myOpenData { get; }
    void CountDown(bool WithinRange, iVTank tankies);
    void CountDown(bool WithinRange, iCharacterHandler mobs);
    void ResetCounter();
    void SetHealth(float v, bool increment);
    void SetHealth(float v, bool increment, float timer);
    void OnVacuumState(iController icon, bool v);
    BonusStats myBonusData { get; }
    void DoStartGame();
}



public class CharacterHandler : MonoBehaviour, iCharacterHandler
{
    public bool isDown { get; private set; }

    [SerializeField] public CharacterData myInitData;
    [SerializeField] Image Counter;
    [SerializeField] bool ActivateCounter;
    [SerializeField] Vector3 ParticleOffset;
    [SerializeField] float _FlashDuration;
    [SerializeField] SkinnedMeshRenderer BodyRenderer;

    Material m_Body;
    float flashLerp = 2f;
    float CurAttackTimer = 0f;
    float curHealthDownTimer = 0f;
    float CurAttackTimerReset = 0.8f;
    float CurAttackTimerResetCounter = 0f;
    float VacuumParticleSpawnInterval = .2f;
    float VacuumParticleTimer = 0f;
    float CurDownTimer = 0f;
    float MyCurHealth = 0;
    float internalVacuumDownTime = 1.2f;
    float internalVacuumDownCounter = 0f;
    bool _OnVacuum = false;
    bool _scored = true;
    Controller myController;
    CharacterAnimator myCharAnim;
    CharacterHandlerProcessor chp;
    EnemySpawner ENS;
    PlayerCombatHandler pch;
    PowerUpHolder Puh;
    CharacterData iCharacterHandler.myOpenData => myInitData;

    BonusStats iCharacterHandler.myBonusData { get => Puh.bonus; }
    MusicPlayer mPlayer;

    float _tempAtkCounter = 0f;
    Transform _target;

    Coroutine FlashingCoroutine;

    void Start()
    {
        myController = GetComponent<Controller>();
        myCharAnim = GetComponent<CharacterAnimator>();
        mPlayer = GameObject.FindGameObjectWithTag("Sound Manager").GetComponent<MusicPlayer>();

        _target = GameObject.FindGameObjectWithTag("Target").transform;
        chp = new CharacterHandlerProcessor();
        CurAttackTimer = myInitData.DeliverDamageTimer;
        CurAttackTimerResetCounter = CurAttackTimerReset;
        MyCurHealth = myInitData.CharacterHealth;

        if (myInitData.CharacterType == CharacterTypes.Player)
        {
            pch = GetComponent<PlayerCombatHandler>();
            Puh = GetComponent<PowerUpHolder>();

        }

        internalVacuumDownCounter = internalVacuumDownTime;
        isDown = false;

        GetMaterial();
        GetEnemySpawner();

        VacuumParticleTimer = VacuumParticleSpawnInterval;
    }

    private void GetEnemySpawner()
    {
        ENS = GameObject.FindGameObjectWithTag("Manager").GetComponent<EnemySpawner>();
    }

    private void GetMaterial()
    {
        if (myInitData.CharacterType == CharacterTypes.Player)
        {
            //player
            m_Body = BodyRenderer.materials[0];
        }
        else if (myInitData.CharacterType == CharacterTypes.Boss || myInitData.CharacterType == CharacterTypes.Mob)
        {
            //non-player
            m_Body = BodyRenderer.materials[0];
            // m_Body = BodyRenderer.materials[0];
        }
    }

    void FixedUpdate()
    {
        if (isDown) return;
        chp.DoMove(myController, this, _target, myInitData.CharacterType);
        chp.DoVacuum(this, myController, myCharAnim, pch);
    }

    private void Update()
    {
        //   if (myInitData.CharacterType == CharacterTypes.Mob && Time.time % 2 == 0)
        //   {
        VacuumUpgradeListener();
        VacuumDownListener();
        HealthListener();
        AttackTimerCounterListener();
        OnDownCounter();
        ParticleListener();
        //    }
    }

    private void ParticleListener()
    {
        if (myInitData.CharacterType == CharacterTypes.Mob || myInitData.CharacterType == CharacterTypes.Boss)
        {
            if (_OnVacuum)
            {
                if (VacuumParticleTimer <= 0f)
                {
                    ENS.SpawnParticle(8, transform.position + ParticleOffset);
                    VacuumParticleTimer = VacuumParticleSpawnInterval;
                }

                VacuumParticleTimer -= Time.deltaTime;
            }
        }
    }

    private void VacuumUpgradeListener()
    {
        if (myInitData.CharacterType == CharacterTypes.Player)
        {
            if (MixedHandler.Instance.onUpgradeVacuum)
            {
                chp.SetPower(pch, Puh.bonus.AddedPower, true);
                chp.SetRange(pch, Puh.bonus.AddedRange, true);
                MixedHandler.Instance.onUpgradeVacuum = false;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (myInitData.CharacterType == CharacterTypes.Player)
        {
            if (isDown) return;
            if (MixedHandler.Instance.AllFreeze) return;
            if (collision.gameObject.layer.Equals(LayerMask.NameToLayer("Hostile")))
            {
                ENS.SpawnParticle(9, transform.position + ParticleOffset);
                Flashing();
                var g = collision.gameObject.GetComponent<CharacterHandler>();
                if (g)
                {
                    CharacterTypes types = g.myInitData.CharacterType;

                    if (types == CharacterTypes.Mob || types == CharacterTypes.Boss)
                    {
                        chp.SendKnockBackWave(myController, g.myInitData.KnockUpForce, g.myInitData.KnockbackForce);
                        mPlayer.Play("Player Hit1", "EnemySFXSources");
                        mPlayer.Play("Enemy Hit1", "PlayerSFXSources");
                        CurDownTimer = myInitData.DownDuration;
                        isDown = true;
                        chp.DoAnim(myCharAnim, "Stun", true);
                        chp.ResetPowerUp(Puh);
                        chp.ResetVacuum(pch);
                    }
                }
            }
        }
    }

    private void Flashing()
    {
        if (FlashingCoroutine == null)
        {
            FlashingCoroutine = StartCoroutine(FlashingEnumerator());
        }
    }

    private IEnumerator FlashingEnumerator()
    {
        m_Body.SetFloat("_Flashing", 1f);
        yield return new WaitForSeconds(_FlashDuration);
        m_Body.SetFloat("_Flashing", 0f);
        FlashingCoroutine = null;
        yield break;
    }

    private void OnDownCounter()
    {
        if (isDown)
        {
            if (CurDownTimer <= 0)
            {
                isDown = false;
                chp.DoAnim(myCharAnim, "Stun", isDown);
            }

            CurDownTimer -= Time.deltaTime;
            DoCountDownTimer(CurDownTimer, true);
        }

    }

    private void AttackTimerCounterListener()
    {
        if (myInitData.CharacterType == CharacterTypes.Player)
        {
            Counter.gameObject.SetActive(isDown);
        }

        if (ActivateCounter)
        {
            if (CurAttackTimer == myInitData.DeliverDamageTimer)
            {
                ActivateCounter = false;
            }
        }

        Counter.gameObject.SetActive(ActivateCounter);
    }

    private void VacuumDownListener()
    {
        if (myInitData.CharacterType == CharacterTypes.Mob)
        {
            if (internalVacuumDownCounter <= 0)
            {
                chp.OnVacuum(this, myController, false);
                _OnVacuum = false;
            }

            internalVacuumDownCounter -= Time.deltaTime;
        }
    }

    void iCharacterHandler.ProcessMove(float v, float h, iController iController)
    {
        var dir = new Vector3(h, 0f, v);

        if (myInitData.CharacterType == CharacterTypes.Player)
        {
            if (MixedHandler.Instance.GameIsOver)
            {
                if (MixedHandler.Instance.WeWon)
                {
                    if (!chp.GetAnim(myCharAnim, "Win"))
                    {
                        chp.DoAnim(myCharAnim, "Win", true);
                    }
                }
                else
                {
                    if (!chp.GetAnim(myCharAnim, "Lose"))
                    {
                        chp.DoAnim(myCharAnim, "Lose", true);
                    }
                }
                return;
            }
        }

        chp.DoAnim(myCharAnim, "Velocity", dir.magnitude);

        if (myInitData.CharacterType != CharacterTypes.Player)
            chp.DoAnim(myCharAnim, "Vacuumed", _OnVacuum);

        if (iController.MovementType == MovementType.NavMesh)
        {
            if (myInitData.CharacterType == CharacterTypes.Boss)
            {
                // chp.ChangeTargetNavigation(myController);
            }

            iController.isFreezing();
            return;
        }

        iController.CalcSlopeAngleDetection();
        iController.AutoDetectSlope();
        iController.CalcDirection(v, h);

        chp.SetTurnRate(myController, _OnVacuum);
        if (_OnVacuum) return;
        iController.MoveToPosition(dir);
    }

    private void DoCountDownTimer(float val, bool isCountStun)
    {
        if (!ActivateCounter) return;
        var v = Convert(val, 0, isCountStun ? myInitData.DownDuration : myInitData.DeliverDamageTimer, 0, 1f);
        Counter.fillAmount = Mathf.Clamp(v, 0f, isCountStun ? myInitData.DownDuration : myInitData.DeliverDamageTimer);
    }

    float Convert(float curVal, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (((curVal - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    void iCharacterHandler.CountDown(bool WithinRange, iCharacterHandler Char)
    {
        if (WithinRange)
        {
            if (CurAttackTimer <= 0)
            {
                chp.DoDamage(this, Char, false);

                CurAttackTimer = myInitData.DeliverDamageTimer;
            }
            CurAttackTimer -= Time.deltaTime;

            return;
        }
    }

    void iCharacterHandler.CountDown(bool WithinRange, iVTank tankies)
    {
        if (MixedHandler.Instance.AllFreeze || MixedHandler.Instance.BossEntrance) return;

        if (WithinRange)
        {
            _tempAtkCounter = CurAttackTimer;

            ActivateCounter = true;

            MixedHandler.Instance.SetCurrentDamageTaken(myInitData.CharacterAttack.ToString());

            if (CurAttackTimer <= .011f)
            {
                MixedHandler.Instance.SpawnDamageCanvas(MixedHandler.Instance._flag, 2);
            }

            if (CurAttackTimer <= .01f)
            {
                chp.DoDamage(tankies, this);
                if (myInitData.CharacterType == CharacterTypes.Mob)
                {
                    chp.DoDamage(9999f, this, false);
                    _scored = false;
                    mPlayer.Play("Player Hit2", "EnemySFXSources");
                }
                CurAttackTimer = myInitData.DeliverDamageTimer;

            }
            var g = CurAttackTimer -= Time.deltaTime;
            DoCountDownTimer(g, false);
            return;
        }
        else
        {
            ActivateCounter = false;
        }
    }

    void iCharacterHandler.ResetCounter()
    {
        CurAttackTimer = myInitData.DeliverDamageTimer;
    }

    void iCharacterHandler.SetHealth(float v, bool increment)
    {
        MyCurHealth += increment ? v : -v;
    }

    void iCharacterHandler.SetHealth(float v, bool increment, float timer)
    {
        curHealthDownTimer = curHealthDownTimer <= 0 ? timer : curHealthDownTimer;

        Flashing();

        MixedHandler.Instance.SetCurrentDamageDealt(v.ToString());
        if (curHealthDownTimer < .011f)
        {
            MixedHandler.Instance.SpawnDamageCanvas(transform);

            if (myInitData.CharacterType == CharacterTypes.Player)
            {
                mPlayer.Play("Enemy Hit1", "SFXSources");
            }
            else if (myInitData.CharacterType == CharacterTypes.Boss || myInitData.CharacterType == CharacterTypes.Mob)
            {
                mPlayer.Play("Player Hit1", "EnemySFXSources");
            }
        }

        if (curHealthDownTimer <= .01f)
        {
            MyCurHealth += increment ? v : -v;
        }
        curHealthDownTimer -= Time.deltaTime;
    }

    void HealthListener()
    {
        if (MyCurHealth <= 0)
        {
            if (myInitData.CharacterType == CharacterTypes.Mob || myInitData.CharacterType == CharacterTypes.Boss)
            {
                if (MixedHandler.Instance.Boss.Contains(this))
                {
                    MixedHandler.Instance.Boss.Remove(this);
                }

                Destroy(gameObject);
            }
        }
    }

    void iCharacterHandler.OnVacuumState(iController icon, bool v)
    {
        if (v)
        {
            internalVacuumDownCounter = internalVacuumDownTime;
        }

        icon.OnVacuumState(v);
        _OnVacuum = v;
    }

    void iCharacterHandler.DoStartGame()
    {
        chp.DoAnim(myCharAnim, "Start", true);
    }

    private void OnDisable()
    {
        try
        {
            if (myInitData.CharacterType == CharacterTypes.Boss)
            {
                mPlayer.Play("Enemy Vacuum", "EnemySFXSources");

                MixedHandler.Instance.BossScore += (int)myInitData.MobScore;
            }
            else if (myInitData.CharacterType == CharacterTypes.Mob)
            {
                mPlayer.Play("Enemy Vacuum", "EnemySFXSources");

                if (_scored)
                    MixedHandler.Instance.MobScore += (int)myInitData.MobScore;
            }
        }
        catch (Exception)
        {

        }

    }

}

public class CharacterHandlerProcessor
{
    internal void SetStart(iCharacterHandler icha)
    {
        icha.DoStartGame();
        MixedHandler.Instance.GameStart = true;
        MixedHandler.Instance.DoSpawnEnemy = true;
    }

    internal void DoVacuum(iCharacterHandler icha, iController iCon, iCharAnim ichara, iHaveVacuum iva)
    {
        if (icha.myOpenData.CharacterType != CharacterTypes.Player) return;
        var isVacuum = MixedHandler.Instance.GameIsOver ? false : iCon.GetVacuumButton();


        ichara.SetBool(isVacuum, "Vacuuming");
        icha.OnVacuumState(iCon, isVacuum);
        iva.DoVacuum(isVacuum);
    }

    internal void OnVacuum(iCharacterHandler icha, iController iCon, bool t)
    {
        icha.OnVacuumState(iCon, t);
    }

    internal void DoMove(iController iController, iCharacterHandler iCharHandler, Transform _target, CharacterTypes types)
    {

        var _h = iController.GetHorizontal();
        var _v = iController.GetVertical();

        var _player = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<CharacterHandler>();

        switch (iController.MovementType)
        {
            case MovementType.Controller:
                _h = iController.GetHorizontal();
                _v = iController.GetVertical();
                break;

            case MovementType.NavMesh:

                if (types == CharacterTypes.Boss)
                {
                    // iController.CalcNavigation(_target, _player);
                }
                else
                {
                    iController.NavmeshInitializing();
                    iController.CalcNavigation(_target);
                }
                break;

            case MovementType.Simple:
                iController.SimpleInitializing();
                if (types == CharacterTypes.Boss)
                {
                    //  iController.CalcNavigation(_target, _player);
                }
                else
                {
                    iController.CalcNavigation(_target);
                }
                var dir = iController.CalcTargetDirection();
                _h = dir.normalized.x;
                _v = dir.normalized.y;
                break;
        }

        iCharHandler.ProcessMove(_v, _h, iController);
    }

    internal void ProcessingDamage(iCharacterHandler v, iVTank virusTankHandler, Collider c)
    {
        v.CountDown(virusTankHandler.isWithinRange(c), virusTankHandler);
    }

    internal void DoAnim(iCharAnim iCharAnimation, string name, float v)
    {
        iCharAnimation.SetFloat(v, name);
    }

    internal void DoAnim(iCharAnim iCharAnimation, string name, bool v)
    {
        iCharAnimation.SetBool(v, name);
    }

    internal bool GetAnim(iCharAnim ichara, string name)
    {
        return ichara.GetBool(name);
    }

    internal float TimeDoDamageCount(iCharacterHandler icha)
    {
        return icha.myOpenData.DeliverDamageTimer;
    }

    internal void DoDamage(iVTank tankies, iCharacterHandler icha)
    {
        tankies.ChangeMyDurability(icha.myOpenData.CharacterAttack);
    }

    internal void DoDamage(iCharacterHandler icha, iCharacterHandler target, bool increment)
    {
        target.SetHealth(icha.myOpenData.CharacterAttack + icha.myBonusData.AddedAttack, increment);
    }

    internal void DoDamage(float val, iCharacterHandler target, bool increment)
    {
        target.SetHealth(val, increment);
    }

    internal void DoDamage(iCharacterHandler icha, iCharacterHandler target, bool increment, bool timer)
    {
        if (timer)
        {
            target.SetHealth(icha.myOpenData.CharacterAttack + icha.myBonusData.AddedAttack, increment, icha.myOpenData.DeliverDamageTimer);
        }
        else
        {
            target.SetHealth(icha.myOpenData.CharacterAttack + icha.myBonusData.AddedAttack, increment);
        }
    }

    internal void resetCounter(iCharacterHandler icha)
    {
        icha.ResetCounter();
    }

    internal void SetTurnRate(iController icon, bool v)
    {
        icon.SetTurnRateState(v);
    }

    internal void SendKnockBackWave(iController myController, float up, float back)
    {
        myController.SendKnockBack(back, up);
    }

    internal void ChangeTargetNavigation(iController icon)
    {
        icon.ChangeTargetNavigation();
    }

    internal void SetPower(iHaveVacuum iva, float v, bool increment)
    {
        iva.SetPower(v, increment);
    }

    internal void SetRange(iHaveVacuum iva, float v, bool increment)
    {
        iva.SetRange(v, increment);
    }

    internal void ResetPowerUp(iAmPowerUp ipul)
    {
        ipul.ResetPowerUp();
    }

    internal void ResetVacuum(iHaveVacuum pch)
    {
        pch.ResetPower();
    }
}
