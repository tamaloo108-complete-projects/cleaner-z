﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUDUpdater : MonoBehaviour
{
    [Header("Durability HUD Settings")]
    [SerializeField] bool useBar;
    [SerializeField] Image DurabilityFiller;
    [SerializeField] Image DurabilityBaseOverlay;
    [SerializeField] VirusTankHandler Flag;
    [SerializeField] EnemySpawner Mobs;
    [SerializeField] TextMeshProUGUI DurabiltyNum;
    [SerializeField] float BaseOverlayTimeDelta = 0.25f;
    [SerializeField] float NumberTimeStep = 0.2f;


    [Header("Wave Counter Settings")]
    [SerializeField] TextMeshProUGUI MobCounterNum;

    float curHealthText = 0;
    float curMob = 0;
    float MaxMob = 0;

    private void Start()
    {
        UpdateDurabiltyText(Flag.MaxHealth);
        MaxMob = Mobs._WavesMobs;
        curMob = MaxMob;
        UpdateWaveText(MaxMob);
    }

    private void UpdateWaveText(float max)
    {
        curMob = Mathf.MoveTowards(curMob, max, NumberTimeStep * Time.deltaTime);
        //var fill = Mathf.Clamp(curHealthText, 0, curHealth);
        var _fill = Mathf.FloorToInt(curMob);
        MobCounterNum.SetText(_fill.ToString());
    }

    void Update()
    {
        if (useBar)
        {
            UpdateDurabilityFiller(Flag.CurHealth, Flag.MaxHealth);
            UpdateDurabilityOverlay(DurabilityFiller.fillAmount);
        }
        UpdateDurabiltyText(Flag.CurHealth);
        UpdateWaveText(Mobs._WavesMobs);
    }

    private void UpdateDurabiltyText(float curHealth)
    {
        curHealthText = Mathf.MoveTowards(curHealthText, curHealth, NumberTimeStep * Time.deltaTime);
        //var fill = Mathf.Clamp(curHealthText, 0, curHealth);
        var _fill = Mathf.FloorToInt(curHealthText);
        DurabiltyNum.SetText(_fill.ToString());
    }

    private void UpdateDurabilityOverlay(float fillAmount)
    {
        if (DurabilityBaseOverlay.fillAmount != fillAmount)
        {
            var fill = Mathf.MoveTowards(DurabilityBaseOverlay.fillAmount, fillAmount, Time.deltaTime * BaseOverlayTimeDelta);
            DurabilityBaseOverlay.fillAmount = fill;
        }
    }

    private void UpdateDurabilityFiller(float curHealth, float MaxHealth)
    {
        var _v = Convert(curHealth, 0, MaxHealth, 0, 1);
        DurabilityFiller.fillAmount = _v;
    }

    float Convert(float curVal, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (((curVal - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }
}
