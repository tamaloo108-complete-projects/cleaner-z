﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDetector : MonoBehaviour
{
    List<Collider> DetectedObject = new List<Collider>();
    Collider myCollider;
    [SerializeField] Transform AnimatedScaleTransform;
    float baseX = 25f, baseY = 25f, baseZ = 25f;
    float initX = 50f, initY = 50f, initZ = 50f;

    private void Start()
    {
        myCollider = GetComponent<Collider>();
    }

    public void SetDustSize(Transform p, int level, bool increment)
    {
        float _x = transform.localScale.x;
        float _y = transform.localScale.y;
        float _z = transform.localScale.z;
        if (level % 2 == 0) //increase x & y value
        {
            _x += increment ? baseX : -baseX;
            _y += increment ? baseY : -baseY;
        }

        _z += increment ? baseZ : -baseZ;

        p.localScale = new Vector3(_x, _y, _z);
    }

    public void SetConeSize(int level, bool increment)
    {
        float _x = transform.localScale.x;
        float _y = transform.localScale.y;
        float _z = transform.localScale.z;
        if (level % 2 == 0) //increase x & y value
        {
            _x += increment ? baseX : -baseX;
            _y += increment ? baseY : -baseY;
        }

        _z += increment ? baseZ : -baseZ;

        transform.localScale = new Vector3(_x, _y, _z);
        AnimatedScaleTransform.localScale = new Vector3(_x, _y, _z);
    }

    public void ResetConeSize()
    {
        transform.localScale = new Vector3(initX, initY, initZ);
        AnimatedScaleTransform.localScale = new Vector3(initX, initY, initZ);
    }

    public List<Collider> GetDetectedObjects()
    {
        return DetectedObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        DetectedObject.RemoveAll(x => x == null);
        try
        {
            if (other.tag.Equals("Physics"))
            {
                if (!DetectedObject.Contains(other))
                    DetectedObject.Add(other);
                //succ
            }
        }
        catch (System.Exception)
        {

        }

    }

    private void OnTriggerStay(Collider other)
    {
        DetectedObject.RemoveAll(x => x == null);
        try
        {
            if (other.tag.Equals("Physics"))
            {
                if (!DetectedObject.Contains(other))
                    DetectedObject.Add(other);
                //succ
            }
        }
        catch (System.Exception)
        {

        }

    }

    private void OnTriggerExit(Collider other)
    {
        DetectedObject.RemoveAll(x => x == null);
        try
        {
            if (other.tag.Equals("Physics"))
            {
                if (DetectedObject.Contains(other))
                    DetectedObject.Remove(other);
                //dont succ
            }

            DetectedObject = new List<Collider>();
        }
        catch (System.Exception)
        {

            // throw;
        }

    }
}
