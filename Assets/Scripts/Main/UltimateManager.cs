﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class UltimateManager : MonoBehaviour
{
    [Header("Effect Settings")]
    [SerializeField] Animator EffectAnimator;

    [SerializeField] Image FillerUltimate;
    [SerializeField] float MaxBar = 25f;
    [SerializeField] float CurUltimateBar = 0f;
    [SerializeField] float FillPerTick = 0.25f;
    [SerializeField] float FreezeDuration = 4f;
    bool Ultimate = false;

    Coroutine FreezeDurationCoroutine;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FillTheBar());
    }

    private void Update()
    {
        DoUltimate();
    }

    private void DoUltimate()
    {
        if (CrossPlatformInputManager.GetButtonDown("Ultimate") && Ultimate)
        {
            if (FreezeDurationCoroutine == null)
            {
                EffectAnimator.SetBool("Active", true);
                Ultimate = false;
                MixedHandler.Instance.AllFreeze = true;
                FreezeDurationCoroutine = StartCoroutine(Freezing());
            }
        }
    }

    private IEnumerator Freezing()
    {

        var k = FreezeDuration;
        while (k >= 0)
        {
            yield return new WaitForSeconds(1f);
            var val = Convert(k, 0f, FreezeDuration, 0, 1f);
            FillerUltimate.fillAmount = val;
            k -= 1f;

        }
        MixedHandler.Instance.AllFreeze = false;
        CurUltimateBar = 0f;
        FreezeDurationCoroutine = null;
        EffectAnimator.SetBool("Active", false);
        yield break;
    }

    private IEnumerator FillTheBar()
    {
        while (true)
        {
            yield return new WaitUntil(() => MixedHandler.Instance.GameStart);
            yield return new WaitUntil(() => !MixedHandler.Instance.GameOnPause);
            yield return new WaitUntil(() => !MixedHandler.Instance.AllFreeze);
            if (CurUltimateBar == MaxBar)
            {
                Ultimate = true;
                yield return new WaitUntil(() => CurUltimateBar <= MaxBar);
            }
            CurUltimateBar += FillPerTick;
            var val = Convert(CurUltimateBar, 0, MaxBar, 0, 1f);
            FillerUltimate.fillAmount = val;
            yield return new WaitForSeconds(1f);
        }
    }

    float Convert(float curVal, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (((curVal - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }
}
