﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


internal interface iAmPowerUp
{
    void ResetPowerUp();
}

public class PowerUpHolder : MonoBehaviour, iAmPowerUp
{
    List<PowerUpTypes> PowerGained = new List<PowerUpTypes>();
    public BonusStats bonus { get; private set; }
    [SerializeField] PowerUpManager Puma;

    [SerializeField] float AttackGain = 0.5f;
    [SerializeField] float PowerGain = 0.025f;
    [SerializeField] float SpeedGain = 0.045f;
    [SerializeField] int RangeGain = 1;

    float AddedAttack = 0f;
    float AddedPower = 0f;
    float AddedSpeed = 0f;
    int AddedRange = 0;

    CharacterHandlerProcessor chap;

    private void Start()
    {
        chap = new CharacterHandlerProcessor();
        bonus = new BonusStats(AddedAttack, AddedPower, AddedSpeed, AddedRange);
        Puma = GameObject.FindGameObjectWithTag("Manager").GetComponent<PowerUpManager>();
    }

    internal void AddPowerUp(PowerUpTypes powerUpPreference)
    {
        var item = powerUpPreference;
        if (item == PowerUpTypes.Heal)
        {
            MixedHandler.Instance.OnHeal = true;
        }

        if (item != PowerUpTypes.Heal)
        {
            PowerGained.Add(item);
        }
        calibratePowerUp(item);
        bonus = new BonusStats(AddedAttack, AddedPower, AddedSpeed, AddedRange);
    }

    private void calibratePowerUp(PowerUpTypes item)
    {
        switch (item)
        {
            case PowerUpTypes.AttackUp:
                AddedAttack += AttackGain;
                break;

            case PowerUpTypes.PowerUp:
                AddedPower += PowerGain;
                break;

            case PowerUpTypes.RangeUp:
                AddedRange += RangeGain;
                break;

            case PowerUpTypes.SpeedUp:
                AddedSpeed += SpeedGain;
                break;

            case PowerUpTypes.Heal:
                //heal
                break;
        }
    }

    private void calibratePowerUp()
    {
        AddedAttack = 0f;
        AddedPower = 0f;
        AddedSpeed = 0f;
        AddedRange = 0;

        foreach (var item in PowerGained)
        {
            switch (item)
            {
                case PowerUpTypes.AttackUp:
                    AddedAttack += AttackGain;
                    break;

                case PowerUpTypes.PowerUp:
                    AddedPower += PowerGain;
                    break;

                case PowerUpTypes.RangeUp:
                    AddedRange += RangeGain;
                    break;

                case PowerUpTypes.SpeedUp:
                    AddedSpeed += SpeedGain;
                    break;

                case PowerUpTypes.Heal:
                    //heal
                    break;
            }
        }
    }

    private void ResetPowerUp()
    {

        PowerGained = new List<PowerUpTypes>();
        AddedAttack = 0f;
        AddedPower = 0f;
        AddedRange = 0;
        AddedSpeed = 0f;

        bonus = new BonusStats(AddedAttack, AddedPower, AddedSpeed, AddedRange);
    }

    void iAmPowerUp.ResetPowerUp()
    {
        AnimatedLoss();
    }

    private void AnimatedLoss()
    {
        //Resources.UnloadUnusedAssets();
        //System.GC.Collect();
        Debug.LogError("power count :" + PowerGained.Count);
        if (PowerGained == null) return;
        foreach (var item in PowerGained)
        {
            Puma.Spawn(item, transform, true);
            //PowerGained.Remove(item);
        }

        //PowerGained.Clear();

        ResetPowerUp();
    }
}


public class BonusStats
{
    public float AddedAttack { get; private set; }
    public float AddedPower { get; private set; }
    public float AddedSpeed { get; private set; }
    public int AddedRange { get; private set; }

    public BonusStats(float addedAttack, float addedPower, float addedSpeed, int addedRange)
    {
        AddedAttack = addedAttack;
        AddedPower = addedPower;
        AddedSpeed = addedSpeed;
        AddedRange = addedRange;
    }
}
