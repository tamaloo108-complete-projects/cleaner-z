﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface iVTank
{
    void ChangeMyDurability(float v);
    bool isWithinRange(Collider c);
}

public class VirusTankHandler : MonoBehaviour, iVTank
{
    [SerializeField] Vector3 BoxcastSize;
    [SerializeField] Vector3 BoxcastOffset;
    [SerializeField] LayerMask EnemyLayer;
    [SerializeField] VirusTankData TankData;

    public float CurHealth { get; private set; }
    public float MaxHealth { get; private set; }
    bool isActive = true;
    Collider[] HostileMobCollider = new Collider[100];
    CharacterHandlerProcessor chap;
    List<Collider> MobDetected = new List<Collider>();

    Animator TankAnim;

    Coroutine MobDetectorCoroutine;
    private float BreakDistance = 1.2f;
    private float _velocity;
    [SerializeField] private float _smoothTime = 0.2f;

    private void OnEnable()
    {
        CurHealth = TankData.Durability;
        MaxHealth = TankData.Durability;
    }

    // Start is called before the first frame update
    void Start()
    {
        TankAnim = GetComponent<Animator>();
        chap = new CharacterHandlerProcessor();
        if (MobDetectorCoroutine == null)
            MobDetectorCoroutine = StartCoroutine(MobDetector());

        MixedHandler.Instance.SetFlag(transform);

        StartCoroutine(IdleAnimator());
    }

    private IEnumerator IdleAnimator()
    {
        while (true)
        {
            try
            {
                if (HostileMobCollider.ToList().Find(x => x == null) && TankAnim.GetBool("Damaged"))
                {
                    TankAnim.SetBool("Damaged", false);
                }
            }
            catch (Exception)
            {
                //       throw;
            }
            float k = UnityEngine.Random.Range(0f, 1f);
            var g = Mathf.SmoothDamp(TankAnim.GetFloat("Rating"), k, ref _velocity, _smoothTime);
            TankAnim.SetFloat("Rating", g);
            yield return new WaitForSeconds(Time.deltaTime);
        }

    }

    private IEnumerator MobDetector()
    {

        while (true)
        {
            if (CurHealth <= 0)
            {
                MixedHandler.Instance.GameIsOver = true;
            }

            if (MixedHandler.Instance.OnHeal)
            {
                CurHealth += 3;
                MixedHandler.Instance.OnHeal = false;
            }
            yield return new WaitUntil(() => isActive);
            MobDetect();
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void MobDetect()
    {
        try
        {

            Physics.OverlapBoxNonAlloc(transform.position + BoxcastOffset, BoxcastSize, HostileMobCollider, transform.rotation, EnemyLayer);
            foreach (var t in HostileMobCollider)
            {
                CharacterHandler v = t.gameObject.GetComponent<CharacterHandler>();
                //Debug.Log(Vector3.Distance(transform.position + BoxcastOffset, t.transform.position));
                if (Vector3.Distance(transform.position + BoxcastOffset, t.transform.position) < BreakDistance)
                {
                    chap.ProcessingDamage(v, this, t);
                    TankAnim.SetBool("Damaged", true);
                }
                else
                {
                    chap.resetCounter(v);

                    if (MobDetected.Count == 0)
                    {
                        TankAnim.SetBool("Damaged", false);
                    }

                    TankAnim.SetBool("Damaged", false);
                }

            }
        }
        catch (Exception e)
        {
            // Debug.Log("hiis " + e.Message);
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position + BoxcastOffset, BoxcastSize);
    }

    void iVTank.ChangeMyDurability(float v)
    {
        CurHealth -= v;
    }

    bool iVTank.isWithinRange(Collider c)
    {
        return HostileMobCollider.ToList().Find(x => x == c);
    }
}
