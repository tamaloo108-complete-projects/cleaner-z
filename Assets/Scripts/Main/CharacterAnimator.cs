﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimator : MonoBehaviour, iCharAnim
{
    Animator myAnim;

    void Start()
    {
        myAnim = GetComponent<Animator>();
    }

    bool iCharAnim.GetBool(string name)
    {
        return myAnim.GetBool(name);
    }

    float iCharAnim.GetFloat(string name)
    {
        return myAnim.GetFloat(name);
    }

    void iCharAnim.SetBool(bool v, string name)
    {
        myAnim.SetBool(name, v);
    }

    void iCharAnim.SetFloat(float v, string name)
    {
        myAnim.SetFloat(name, v);
    }

}

internal interface iCharAnim
{
    void SetFloat(float v, string name);
    void SetBool(bool v, string name);
    float GetFloat(string name);
    bool GetBool(string name);
}