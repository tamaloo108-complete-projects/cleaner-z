﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MixedHandler : Singleton<MixedHandler>
{
    public bool DoSpawnEnemy = false;
    public bool BossEntrance = false;
    public bool GameStart = false;
    public bool GameOnPause = false;
    public bool onUpgradeVacuum = false;
    public bool AllFreeze = false;
    public string curDmgText { get; private set; }
    public string curDmgTankTaken { get; private set; }
    EnemySpawner Enes;
    public Transform _player { get; private set; }
    public Transform _flag { get; private set; }
    public bool OnHeal { get; internal set; }
    public bool WeWon = false;
    public List<CharacterHandler> Boss = new List<CharacterHandler>();
    public int Scores = 0;
    public int MobScore = 0;
    public int BossScore = 0;
    internal bool GameIsOver;

    public void SetPlayer(Transform p)
    {
        _player = p;
    }

    public void SetFlag(Transform p)
    {
        _flag = p;
    }

    public void SetEnemySpawner(EnemySpawner t)
    {
        Enes = t;
    }

    internal void SpawnDamageCanvas(Transform p)
    {
        Enes.SpawnParticle(1, p);
    }

    internal void SpawnDamageCanvas(Transform p, int index)
    {
        Enes.SpawnParticle(index, p);
    }

    public void SetCurrentDamageDealt(string n)
    {
        curDmgText = n;
    }

    public void SetCurrentDamageTaken(string n)
    {
        curDmgTankTaken = n;
    }
}
