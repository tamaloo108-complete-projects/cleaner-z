﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class GameSettingsHandler : MonoBehaviour
{
    [SerializeField] GameSettingsData GameData;
    [SerializeField] Volume _Bloom;
    [SerializeField] Volume _Main;
    [SerializeField] AudioMixer myMixer;
    [SerializeField] GameObject OptionPrefab;
    [SerializeField] GameObject HelpPrefab;

    GameObject OptionInstance, HelpInstance;
    OptionValueSetter OVS;

    float MasterValue;
    float BGMValue;
    float SFXValue;

    void Start()
    {
        SetMixer();
        ApplyQualitySettings();
        ApplyVolumeControl();
        ApplyPostProcessing();
    }

    private void SetMixer()
    {
        SoundManager.Instance.MyAudioMixer = myMixer;
    }

    #region Main

    public void OptionMenuState(bool show)
    {
        if ((OptionInstance == null))
        {
            //Spawn Menu;
            SpawnOptionMenu();
        }
        else if (OptionInstance != null)
        {
            //enable menu;
            OptionInstance.SetActive(show);
        }
    }

    private void ApplyPostProcessing()
    {
        if (SceneManager.GetActiveScene().name == "Title") return;

        _Bloom.enabled = GameData.Bloom;
        _Main.enabled = GameData.PostProcessing;
    }

    private void ApplyVolumeControl()
    {
        SoundManager.Instance.SetMasterVolume(GameData.MasterVolume);
        SoundManager.Instance.SetSFXVolume(GameData.SFXVolume);
        SoundManager.Instance.SetBGMVolume(GameData.BGMVolume);
    }

    private void ApplyQualitySettings()
    {
        QualitySettings.SetQualityLevel((int)GameData.QualitySet);
    }

    #endregion

    #region UI

    public void HelpMenuState(bool show)
    {
        if (HelpInstance == null)
        {
            var g = Instantiate(HelpPrefab);
            HelpInstance = g;
            Button button =  HelpInstance.GetComponent<HelpSectionHandler>().CloseButton;
            button.onClick.AddListener(() => HelpMenuState(false));

            HelpInstance.SetActive(show);
        }
        else if (HelpInstance != null)
        {
            HelpInstance.SetActive(show);
        }
    }

    private void SpawnOptionMenu()
    {
        if (OptionInstance == null)
        {
            var g = Instantiate(OptionPrefab);
            OptionInstance = g;
            OVS = g.GetComponent<OptionValueSetter>();

            OVS._MasterVol.onValueChanged.AddListener((x) => SetMasterVolValue(x));
            OVS._SFXVol.onValueChanged.AddListener((x) => SetSFXVolValue(x));
            OVS._BGMVol.onValueChanged.AddListener((x) => SetBGMVolValue(x));
            OVS.ApplyButton.onClick.AddListener(() => SetUIValue());
            OVS.CloseButton.onClick.AddListener(() => OptionMenuState(false));

            GetUIValue();
        }
    }

    void SetBGMVolValue(float x)
    {
        SoundManager.Instance.SetBGMVolume(x);
    }

    void SetMasterVolValue(float v)
    {
        SoundManager.Instance.SetMasterVolume(v);
    }

    void SetSFXVolValue(float v)
    {
        SoundManager.Instance.SetSFXVolume(v);
    }

    float Convert(float curVal, float oldMin, float oldMax, float newMin, float newMax)
    {
        return (((curVal - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
    }

    void GetUIValue()
    {
        OVS._QualityDropdown.value = (int)GameData.QualitySet;
        OVS._Bloom.isOn = GameData.Bloom;
        OVS._PostProcessing.isOn = GameData.PostProcessing;
        OVS._MasterVol.value = GameData.MasterVolume;
        OVS._BGMVol.value = GameData.BGMVolume;
        OVS._SFXVol.value = GameData.SFXVolume;
    }

    public void SetUIValue()
    {
        GameData.QualitySet = (GameSettingsData.QualityType)OVS._QualityDropdown.value;
        GameData.Bloom = OVS._Bloom.isOn;
        GameData.PostProcessing = OVS._PostProcessing.isOn;
        GameData.MasterVolume = OVS._MasterVol.value;
        GameData.SFXVolume = OVS._SFXVol.value;
        GameData.BGMVolume = OVS._BGMVol.value;

        ApplyQualitySettings();
        ApplyVolumeControl();
        ApplyPostProcessing();
    }

    #endregion
}
