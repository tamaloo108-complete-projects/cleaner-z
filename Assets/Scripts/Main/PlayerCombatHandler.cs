﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


internal interface iHaveVacuum
{
    VacuumData myVacuum { get; }
    void SetPower(float v, bool increment);
    void SetRange(float v, bool increment);
    void DoVacuum(bool v);
    void ResetPower();
}

public class PlayerCombatHandler : MonoBehaviour, iHaveVacuum
{
    [SerializeField] ObjectDetector myZone;
    [SerializeField] VacuumData PlayerVacuum;
    [SerializeField] Vector3 VacuumSize;
    [SerializeField] Vector3 VacuumOffset;
    [SerializeField] float MinimumRange = 0.01f;
    [SerializeField] LayerMask TargetMask;
    [SerializeField] Animator VacuumAnim;
    VacuumData iHaveVacuum.myVacuum => PlayerVacuum;
    float curVacuumPower = 0;
    float curVacuumRange = 0;
    float xMult = 0.64f * 2f;
    float zMult = 3.2f;
    float zOff = -1.5f;
    PowerUpHolder myPowerUp;
    List<Collider> Mobs = new List<Collider>();

    [SerializeField] ParticleSystem Smoke;
    [SerializeField] ParticleSystem Dust;

    CharacterHandler icha;
    CharacterHandlerProcessor chap;
    Vector3 VacuumDirection;
    // Start is called before the first frame update
    void Start()
    {
        myPowerUp = GetComponent<PowerUpHolder>();
        chap = new CharacterHandlerProcessor();
        icha = GetComponent<CharacterHandler>();
        curVacuumPower = PlayerVacuum.VacuumPower;
        curVacuumRange = PlayerVacuum.VacuumRange;
        ModifyRange();


        Smoke.Simulate(0f, false, true, true);
        Dust.Simulate(0f, false, true, true);
        MixedHandler.Instance.SetPlayer(transform);
        ModifyDustSize();
    }



    private void OnDrawGizmos()
    {
        //    Gizmos.color = Color.blue;
        //    Gizmos.matrix = transform.localToWorldMatrix;
        //    Gizmos.DrawWireCube(Vector3.zero - VacuumOffset, VacuumSize);
    }

    void iHaveVacuum.SetPower(float v, bool increment)
    {
        curVacuumPower += increment ? v : -v;
    }


    void iHaveVacuum.SetRange(float v, bool increment)
    {
        curVacuumRange += increment ? v : -v;
        ModifyRange();
        ModifyDustSize();
    }

    private void ModifyDustSize()
    {
        myZone.SetDustSize(Dust.transform, (int)curVacuumRange, true);
    }

    void ModifyRange()
    {
        myZone.SetConeSize((int)curVacuumRange, true);
    }

    void SmokeState(bool v)
    {
        if (v)
        {
            if (!Smoke.isPlaying)
            {
                Smoke.Play();
            }
        }
        else
            Smoke.Stop();
    }

    void DustState(bool v)
    {
        if (v)
            if (!Dust.isPlaying)
            {
                Dust.Play();
            }
            else
                Dust.Stop();
    }

    void ModifyRange(bool Reset)
    {
        if (Reset)
            myZone.ResetConeSize();
        else
            ModifyRange();
    }

    void iHaveVacuum.DoVacuum(bool v)
    {
        try
        {
            Mobs = myZone.GetDetectedObjects();
            VacuumAnim.SetBool("Active", MixedHandler.Instance.GameIsOver ? false : v);
            SmokeState(v);
            DustState(v);
            foreach (var mob in Mobs)
            {
                var icha = mob.GetComponent<CharacterHandler>();
                var icon = mob.GetComponent<Controller>();

                if (v)
                {


                    if (icha != null || icon != null)
                    {
                        chap.OnVacuum(icha, icon, true);
                    }

                    if (Mathf.Abs(Vector3.Distance(transform.position, mob.transform.position)) > MinimumRange)
                    {
                        ApplySuction(mob);
                        if (icha != null || icon != null)
                        {
                            chap.DoDamage(this.icha, icha, false, true);
                        }
                    }
                }
                else
                {
                    if (icha != null || icon != null)
                    {
                        chap.OnVacuum(icha, icon, false);
                    }

                    Mobs = new List<Collider>();
                }
            }
        }
        catch (Exception e)
        {
            //Debug.Log("error on vacuum " + e.Message);
        }
    }

    private void ApplySuction(Collider col)
    {
        var g = col.gameObject.GetComponent<Rigidbody>();
        var mass = g.mass;
        VacuumDirection = transform.position - g.transform.position;
        g.AddForce(VacuumDirection * curVacuumPower * 1 / Vector3.Distance(transform.position, g.transform.position), ForceMode.Impulse);
    }

    void iHaveVacuum.ResetPower()
    {
        curVacuumRange = PlayerVacuum.VacuumRange;
        curVacuumPower = PlayerVacuum.VacuumPower;
        ModifyRange(true);
        ModifyDustSize();
    }
}
