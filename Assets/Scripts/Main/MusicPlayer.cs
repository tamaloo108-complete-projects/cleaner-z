﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] public List<SoundSources> Sources;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "Title")
        {
            Play("Intro", "MusicSources");
        }
        else if (SceneManager.GetActiveScene().name == "Game")
        {
            Play("Selection", "MusicSources");
        }


    }

    AudioSource FindAudioSources(string name)
    {
        return Sources.Find(x => x.name == name).Sources;
    }

    public void Play(int index)
    {
        SoundManager.Instance.Play(index, FindAudioSources("MusicSources"));
    }

    public void Play(string name)
    {
        SoundManager.Instance.Play(name, FindAudioSources("MusicSources"));
    }

    public void Play(string name, string sources)
    {
        SoundManager.Instance.Play(name, FindAudioSources(sources));
    }

    public void Play(int index, string sources)
    {
        SoundManager.Instance.Play(index, FindAudioSources(sources));
    }

    public void PlayScheduled(string name, string sources, double offset)
    {
        SoundManager.Instance.PlayScheduled(name, FindAudioSources(sources), offset);
    }

    public void PlaySE(string name)
    {
        SoundManager.Instance.Play(name, FindAudioSources("SFXSources"));
    }

    public void PlaySE(int index)
    {
        SoundManager.Instance.Play(index, FindAudioSources("SFXSources"));
    }

    public void StopBGM()
    {
        SoundManager.Instance.Stop(FindAudioSources("MusicSources"));
    }

    public void StopSE()
    {
        SoundManager.Instance.Stop(FindAudioSources("SFXSources"));
    }

    public void Stop(string name)
    {
        SoundManager.Instance.Stop(FindAudioSources(name));
    }

}

[System.Serializable]
public class SoundSources
{
    public string name;
    public AudioSource Sources;
}
