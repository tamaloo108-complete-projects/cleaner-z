﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundGather : MonoBehaviour
{
    [SerializeField] List<Sounds> GatheredSounds;

    private void OnEnable()
    {
        if (SoundManager.Instance.MySounds.Count == 0)
            SoundManager.Instance.MySounds = GatheredSounds;
    }
}
