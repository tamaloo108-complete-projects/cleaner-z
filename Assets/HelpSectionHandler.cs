﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpSectionHandler : MonoBehaviour
{
    [SerializeField] List<Sprite> HelpImage;
    [SerializeField] Image TutorialImage;
    public Button CloseButton;
    int curIndex = 0;

    private void Start()
    {
        TutorialImage.sprite = HelpImage[0];
    }

    public void ChangeImage(bool next)
    {
        curIndex = next ? (int)Mathf.MoveTowards(curIndex, HelpImage.Count - 1, 1) : (int)Mathf.MoveTowards(curIndex, 0, 1);
        TutorialImage.sprite = HelpImage[curIndex];
    }


}
